﻿using KirasaModelsDBLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cuirasse
{
    public partial class MainWindow
    {
        /// <summary>
        /// Местоположение на карте оператора
        /// </summary>
        /// <param name="globalPropertiesModel"></param>
        private void DrawLocationOperator(KirasaModelsDBLib.GlobalProperties globalPropertiesModel)
        {
            ucMap.LocationOperator = new WpfMapControl.Location(globalPropertiesModel.Common.OperatorLongitude, globalPropertiesModel.Common.OperatorLatitude);
        }

        /// <summary>
        /// Местоположение на карте Грозы-С
        /// </summary>
        /// <param name="lOtherPoints"></param>
        private void DrawLocationGrozaS(List<TableOtherPoints> lOtherPoints)
        {
            int ind = lOtherPoints.FindIndex(x => x.Type == TypeOtherPoints.GrozaS);
            if (ind != -1)
            {
                ucMap.LocationGrozaS = new WpfMapControl.Location(lOtherPoints[ind].Coordinates.Longitude, lOtherPoints[ind].Coordinates.Latitude); ;
            }
        }

        //private void DrawZones()
        //{
        //ucMap.IsShowZones = basicProperties.Global.Common.ShowZone;

        //if (basicProperties.Global.Common.ShowZone)
        //    ucMap.UpdateMapZones(basicProperties.Global.Common.ZoneAlarm, basicProperties.Global.Common.ZoneAttention, basicProperties.Global.Common.CenterZoneLongitude, basicProperties.Global.Common.CenterZoneLatitude);
        //else
        //    ucMap.ClearMapZones();
        //}
    }
}
