﻿using KirasaModelsDBLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cuirasse
{
    public partial class MainWindow 
    {
        private void InitMap()
        {
            InitLocalPropertyMap();

            ucMap.InitObjectsStyle();

            ucMap.OnChangeFileMap += UcMap_OnChangeFileMap;
            ucMap.OnUpdateZonesCenter += UcMap_OnUpdateZonesCenter;
            ucMap.OnSetLocalReceptionPoint += UcMap_OnSetLocalReceptionPoint;
            ucMap.OnSetRemoteReceptionPoint += UcMap_OnSetRemoteReceptionPoint;
            ucMap.OnShowZones += UcMap_OnShowZones;
            ucMap.OnLocationGrozaS += UcMap_OnLocationGrozaS;
        }

        private void InitLocalPropertyMap()
        {
            ucMap.FileMap = wndViewModel.LocalPropertiesModel.Map.FileMap;
            ucMap.ImageLocalPoint = wndViewModel.LocalPropertiesModel.Map.ImageLocalPoint;
            ucMap.ImageRemotePoint = wndViewModel.LocalPropertiesModel.Map.ImageRemotePoint;
            ucMap.FolderImageSource = wndViewModel.LocalPropertiesModel.Map.FolderImageSource;

            if (ucMap.FolderMapTiles != wndViewModel.LocalPropertiesModel.Map.FolderMapTiles)
            {
                ucMap.FolderMapTiles = wndViewModel.LocalPropertiesModel.Map.FolderMapTiles;
                ucMap.OpenMap();
            }

            ucMap.Projection = (byte)wndViewModel.LocalPropertiesModel.Map.Projection;
            ucMap.LengthOfTrack = wndViewModel.LocalPropertiesModel.Map.LengthOfTrack;
            ucMap.IsAdmin = Convert.ToByte(wndViewModel.LocalPropertiesModel.Common.Access);
            ucMap.AmountOfDefinePoint = wndViewModel.LocalPropertiesModel.Map.AmountOfDefinePoint;
        }

        private void UcMap_OnChangeFileMap(object sender, MapControl.EventPath e)
        {
            wndViewModel.LocalPropertiesModel.Map.FileMap = e.FileMap;
            SetLocalProperties();
            YamlSave(wndViewModel.LocalPropertiesModel);

            ucMap.UpdateMapReceivingPoint(lRemotePoints, NameTable.TableRemotePoints);
            ucMap.UpdateMapReceivingPoint(lLocalPoints, NameTable.TableLocalPoints);
            
            //DrawZones();
        }

        private void UcMap_OnUpdateZonesCenter(object sender, WpfMapControl.Location e)
        {
            wndViewModel.GlobalPropertiesModel.Common.CenterLatitude = Math.Round(e.Latitude, 6);
            wndViewModel.GlobalPropertiesModel.Common.CenterLongitude = Math.Round(e.Longitude, 6);

            SetGlobalProperties();
            GlobalPropertyToDB(basicProperties.Global);

            if (ucMap.IsShowZones)
                ucMap.UpdateMapZones(basicProperties.Global.Common.ZoneAlarm, basicProperties.Global.Common.ZoneAttention, basicProperties.Global.Common.CenterZoneLongitude, basicProperties.Global.Common.CenterZoneLatitude);

        }

        private void UcMap_OnSetLocalReceptionPoint(object sender, WpfMapControl.Location e)
        {
            try
            {
                Coord coord = new Coord
                {
                    Latitude = Math.Round(e.Latitude, 6),
                    Longitude = Math.Round(e.Longitude, 6)
                };

                ucLocalPoints.SetPointToPG(coord);
            }
            catch { }
        }

        private void UcMap_OnSetRemoteReceptionPoint(object sender, WpfMapControl.Location e)
        {
            try
            {
                Coord coord = new Coord
                {
                    Latitude = Math.Round(e.Latitude, 6),
                    Longitude = Math.Round(e.Longitude, 6)
                };

                ucRemotePoints.SetPointToPG(coord);
            }
            catch { }
        }

        private void UcMap_OnShowZones(object sender, bool e)
        {
            try
            {
                if (e)
                    ucMap.UpdateMapZones(basicProperties.Global.Common.ZoneAlarm, basicProperties.Global.Common.ZoneAttention, basicProperties.Global.Common.CenterZoneLongitude, basicProperties.Global.Common.CenterZoneLatitude);
                else
                    ucMap.ClearMapZones();


                //wndViewModel.GlobalPropertiesModel.ShowZone = e;

                //SetGlobalProperties();
                //GlobalPropertyToDB(basicProperties.Global);
            }
            catch { }
        }

        private void UcMap_OnLocationGrozaS(object sender, EventArgs e)
        {
            client.SendExtraMessage(DAPprotocols.ExtraCode.CoordsExchange);
        }
    }
}
