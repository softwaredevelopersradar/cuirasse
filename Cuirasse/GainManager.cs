﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Cuirasse
{
    public partial class MainWindow
    {
        private Gain GainWindow = new Gain();

        private void InitGainWindow()
        {
            GainWindow.NeedGetRequest += Gain_NeedGetRequest;
            GainWindow.NeedSetRequest += Gain_NeedSetRequest;
            GainWindow.NeedHide += Gain_NeedHide;
        }

        private async void Gain_NeedGetRequest(object sender, int EPO, double Gain, GainSettingsCuirasse.GSettings.Devices Devices)
        {
            switch (Devices)
            {
                case GainSettingsCuirasse.GSettings.Devices.Preselector1:

                    var answer1 = await client.GetDeviceGain(DAPprotocols.DapDevice.Preselector1, (byte)EPO);
                    GainWindow.SetGainOnControl(answer1);
                    break;

                case GainSettingsCuirasse.GSettings.Devices.Preselector2:

                    var answer2 = await client.GetDeviceGain(DAPprotocols.DapDevice.Preselector2, (byte)EPO);
                    GainWindow.SetGainOnControl(answer2);
                    break;

                case GainSettingsCuirasse.GSettings.Devices.Preselector3:

                    var answer3 = await client.GetDeviceGain(DAPprotocols.DapDevice.Preselector3, (byte)EPO);
                    GainWindow.SetGainOnControl(answer3);
                    break;

                case GainSettingsCuirasse.GSettings.Devices.Preselector4:

                    var answer4 = await client.GetDeviceGain(DAPprotocols.DapDevice.Preselector4, (byte)EPO);
                    GainWindow.SetGainOnControl(answer4);
                    break;

                case GainSettingsCuirasse.GSettings.Devices.Receiver:

                    var answerR = await client.GetDeviceGain( DAPprotocols.DapDevice.Receiver, (byte)EPO);
                    GainWindow.SetGainOnControl(answerR);
                    break;

                case GainSettingsCuirasse.GSettings.Devices.All:

                    answer1 = await client.GetDeviceGain(DAPprotocols.DapDevice.Preselector1, (byte)EPO);
                    answer2 = await client.GetDeviceGain(DAPprotocols.DapDevice.Preselector2, (byte)EPO);
                    answer3 = await client.GetDeviceGain(DAPprotocols.DapDevice.Preselector3, (byte)EPO);
                    answer4 = await client.GetDeviceGain(DAPprotocols.DapDevice.Preselector4, (byte)EPO);

                    answerR = await client.GetDeviceGain(DAPprotocols.DapDevice.Receiver, (byte)EPO);
                        
                    GainWindow.SetGainOnControl(answer1);
                    GainWindow.SetGainOnControl(answer2);
                    GainWindow.SetGainOnControl(answer3);
                    GainWindow.SetGainOnControl(answer4);

                    GainWindow.SetGainOnControl(answerR);

                    break;

            }
        }

        private async void Gain_NeedSetRequest(object sender, int EPO, double Gain, GainSettingsCuirasse.GSettings.Devices Devices)
        {
            switch (Devices)
            {
                case GainSettingsCuirasse.GSettings.Devices.Preselector1:

                    var answer1 = await client.SetDeviceGain(DAPprotocols.DapDevice.Preselector1, (byte)EPO, (byte)Gain);

                    break;

                case GainSettingsCuirasse.GSettings.Devices.Preselector2:

                    var answer2 = await client.SetDeviceGain(DAPprotocols.DapDevice.Preselector2, (byte)EPO, (byte)Gain);

                    break;

                case GainSettingsCuirasse.GSettings.Devices.Preselector3:

                    var answer3 = await client.SetDeviceGain(DAPprotocols.DapDevice.Preselector3, (byte)EPO, (byte)Gain);

                    break;

                case GainSettingsCuirasse.GSettings.Devices.Preselector4:

                    var answer4 = await client.SetDeviceGain(DAPprotocols.DapDevice.Preselector4, (byte)EPO, (byte)Gain);

                    break;

                case GainSettingsCuirasse.GSettings.Devices.Receiver:

                    var answerR = await client.SetDeviceGain(DAPprotocols.DapDevice.Receiver, (byte)EPO, (byte)Gain);

                    break;
            }
           
        }

        private void SetEPO(int index)
        {
            if (GainToggleButton.IsChecked.Value)
                GainWindow.SetEPO(index);
        }

        private void Gain_NeedHide(object sender)
        {
            GainToggleButton.IsChecked = false;
        }

        private void GainToggleButton_Click(object sender, RoutedEventArgs e)
        {
            if (GainToggleButton.IsChecked.Value)
                GainWindow.Show();
            else
                GainWindow.Hide();
        }
    }
}
