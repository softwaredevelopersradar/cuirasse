﻿using DllCuirassemProperties.Models;
using KirasaModelsDBLib;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using ValuesCorrectLib;
using YamlDotNet.Serialization;

namespace Cuirasse
{
    public partial class MainWindow
    {
        //KirasaModelsDBLib.GlobalProperties globalProperties;
        //LocalProperties localProperties;

        public void InitLocalProperties()
        {
            wndViewModel.LocalPropertiesModel = YamlLoad();

            try
            {
                SetLocalProperties();
                endPoint = wndViewModel.LocalPropertiesModel.DB.IpAddress + ":" + wndViewModel.LocalPropertiesModel.DB.Port;

                ucMap.InitObjectsStyle();
                //basicProperties.DefaultEvent += BasicProperties_DefaultEvent;

                /*LocalPropertyTo3D(wndViewModel.LocalPropertiesModel);*/
                AccessTypes = wndViewModel.LocalPropertiesModel.Common.Access;
            }
            catch { }
        }

        //private void BasicProperties_DefaultEvent(object sender, ControlProperties.BasicProperties.PropertiesType propertiesType)
        //{
        //    switch (propertiesType)
        //    {
        //        case ControlProperties.BasicProperties.PropertiesType.Global:

        //            var defaultGlobalProperties = YamlLoad<GlobalProperties>("DefaultGlobalProperties.yaml");

        //            if (defaultGlobalProperties == null)
        //            {
        //                defaultGlobalProperties = new GlobalProperties()
        //                {
        //                    DetectionFHSS = 0,
        //                    TypeRadioRecon = EnumTypeRadioRecon.WithoutBearing,
        //                    TypeRadioSuppr = EnumTypeRadioSuppr.FWS,

        //                    PingIntervalFHSAP = 5000,

        //                    NumberAveragingPhase = 3,
        //                    NumberAveragingBearing = 3,
        //                    HeadingAngle = 0,
        //                    SignHeadingAngle = true,

        //                    NumberChannels = 4,
        //                    NumberIri = 10,
        //                    OperationTime = 15,
        //                    Priority = 1,
        //                    Threshold = -80,
        //                    TimeRadiateFWS = 5000,

        //                    SectorSearch = 10,
        //                    TimeSearch = 300,

        //                    FFTResolution = 1,
        //                    TimeRadiateFHSS = 900,
        //                };

        //                YamlSave<GlobalProperties>(defaultGlobalProperties, "DefaultGlobalProperties.yaml");
        //            }

        //            basicProperties.Global = defaultGlobalProperties;
        //            break;

        //        case ControlProperties.BasicProperties.PropertiesType.Local:

        //            var defaultLocalProperties = YamlLoad<LocalProperties>("DefaultLocalProperties.yaml");

        //            if (defaultLocalProperties == null)
        //            {
        //                defaultLocalProperties = new LocalProperties();
        //                {
        //                    defaultLocalProperties.ADSB.State = true;
        //                    defaultLocalProperties.ADSB.IpAddress = "192.168.0.11";
        //                    defaultLocalProperties.ADSB.Port = 30005;

        //                    defaultLocalProperties.ARD1.State = true;
        //                    defaultLocalProperties.ARD1.Address = 1;
        //                    defaultLocalProperties.ARD1.ComPort = "COM10";
        //                    defaultLocalProperties.ARD1.PortSpeed = 9600;

        //                    defaultLocalProperties.ARD2.State = true;
        //                    defaultLocalProperties.ARD2.Address = 1;
        //                    defaultLocalProperties.ARD2.ComPort = "COM9";
        //                    defaultLocalProperties.ARD2.PortSpeed = 9600;

        //                    defaultLocalProperties.ARD3.State = true;
        //                    defaultLocalProperties.ARD3.Address = 1;
        //                    defaultLocalProperties.ARD3.ComPort = "COM7";
        //                    defaultLocalProperties.ARD3.PortSpeed = 9600;

        //                    defaultLocalProperties.ARONE.State = true;
        //                    defaultLocalProperties.ARONE.ComPort = "COM5";
        //                    defaultLocalProperties.ARONE.PortSpeed = 19200;

        //                    defaultLocalProperties.CmpPA.State = true;
        //                    defaultLocalProperties.CmpPA.ComPort = "COM12";
        //                    defaultLocalProperties.CmpPA.PortSpeed = 9600;

        //                    defaultLocalProperties.CmpRR.State = true;
        //                    defaultLocalProperties.CmpRR.ComPort = "COM8";
        //                    defaultLocalProperties.CmpRR.PortSpeed = 9600;

        //                    defaultLocalProperties.DbServer.IpAddress = "192.168.0.102";
        //                    defaultLocalProperties.DbServer.Port = 8302;

        //                    defaultLocalProperties.DspServer.IpAddress = "192.168.0.102";
        //                    defaultLocalProperties.DspServer.Port = 10001;

        //                    defaultLocalProperties.EdServer.IpAddress = "192.168.0.102";
        //                    defaultLocalProperties.EdServer.Port = 10009;

        //                    defaultLocalProperties.Tuman.State = false;
        //                    defaultLocalProperties.Tuman.ComPort = "COM11";
        //                    defaultLocalProperties.Tuman.PortSpeed = 9600;
        //                }

        //                YamlSave<LocalProperties>(defaultLocalProperties, "DefaultLocalProperties.yaml");
        //            }

        //            basicProperties.Local = defaultLocalProperties;

        //            break;

        //    }

        //    basicProperties.PerformApply();
        //}

        private void basicProperties_OnGlobalPropertiesChanged(object sender, DllCuirassemProperties.Models.GlobalProperties arg)
        {
            if (wndViewModel.clientDB != null && wndViewModel.clientDB.Tables != null)
            {
                GlobalPropertyToDB(arg);
            }

            SendTableGlobalPropertiesToTechWindow(arg);

            /*GlobalPropertyTo3D(arg);*/

            PanoramaWindow.CorrFunctionControl.UpdateCorrelationTheresold(arg.Correlation.PathThroughCorrelationThreshold);

            if (ucMap.IsShowZones)
                ucMap.UpdateMapZones(basicProperties.Global.Common.ZoneAlarm, basicProperties.Global.Common.ZoneAttention, basicProperties.Global.Common.CenterZoneLongitude, basicProperties.Global.Common.CenterZoneLatitude);

        }

        private void GlobalPropertyToDB(DllCuirassemProperties.Models.GlobalProperties globalProperties)
        {
            MyMapper AutoMapMy = new MyMapper();
            KirasaModelsDBLib.GlobalProperties gp = AutoMapMy.CtrlToDB(globalProperties);
            gp.Id = 1;

            if (wndViewModel.clientDB != null)
            {
                wndViewModel.clientDB?.Tables[NameTable.GlobalProperties].Add(gp);
            }
        }

        private void basicProperties_OnLocalPropertiesChanged(object sender, DllCuirassemProperties.Models.LocalProperties arg)
        {
            endPoint = arg.DB.IpAddress + ":" + arg.DB.Port;
           
            YamlSave(arg);

            SetLanguageTables(basicProperties.Local.Common.Language);
            TranslatorTables.LoadDictionary(basicProperties.Local.Common.Language);
            SetLanguagePanoramas(basicProperties.Local.Common.Language);
            PanoramasGraphs_Access(basicProperties.Local.Common.Access);
            ChangeTechWindowLaguage(basicProperties.Local.Common.Language);
            /*Change3DWindowLanguage(basicProperties.Local.Common.Language);*/

            wndViewModel.LocalPropertiesModel = arg;
            InitLocalPropertyMap();

            AccessTypes = arg.Common.Access;
        }

        private AccessTypes accessTypes;
        public AccessTypes AccessTypes
        {
            get { return accessTypes; }
            set
            {
                //if (accessTypes == value)
                //    return;

                accessTypes = value;
                UpdateTB();
            }
        }

        private void UpdateTB()
        {
            switch(AccessTypes)
            {
                case AccessTypes.Admin:
                    TechToggleButton.Visibility = Visibility.Visible;
                    GainToggleButton.Visibility = Visibility.Visible;
                    StartRecordButton.Visibility = Visibility.Visible;
                    break;

                case AccessTypes.Operator:
                    TechToggleButton.Visibility = Visibility.Hidden;
                    GainToggleButton.Visibility = Visibility.Hidden;
                    StartRecordButton.Visibility = Visibility.Hidden;
                    break;

                default:
                    TechToggleButton.Visibility = Visibility.Hidden;
                    GainToggleButton.Visibility = Visibility.Hidden;
                    StartRecordButton.Visibility = Visibility.Hidden;
                    break;
            }
        }

        private void SetGlobalProperties()
        {
            try
            {
                MyMapper AutoMapMy = new MyMapper();

                basicProperties.Global = AutoMapMy.DBToCtrl(wndViewModel.GlobalPropertiesModel);
            }
            catch
            { }
        }

        private void SetLocalProperties()
        {
            try
            {
                basicProperties.Local = wndViewModel.LocalPropertiesModel;
            }
            catch { }
        }

        private void basicProperties_OnPasswordChecked(object sender, bool e)
        {
            if(!e)
            {
                MessageBox.Show(SMessages.mesPassword, SMessages.mesMessage, MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }
    }
}