﻿using DllCuirassemProperties.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YamlDotNet.Serialization;

namespace Cuirasse
{
    public partial class MainWindow
    {
        public ModelMarkup YamlLoadMarkup()
        {
            string text = "";
            try
            {
                using (StreamReader sr = new StreamReader(AppDomain.CurrentDomain.BaseDirectory + "MarkupWnd.yaml", System.Text.Encoding.Default))
                {
                    text = sr.ReadToEnd();
                    sr.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            var deserializer = new DeserializerBuilder().Build();
            try
            {
                modelMarkup = deserializer.Deserialize<ModelMarkup>(text);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            if (modelMarkup == null)
            {
                modelMarkup = SetDefaultMarkup();
                YamlSaveMarkup(modelMarkup);
            }
            return modelMarkup;
        }
       
        private ModelMarkup SetDefaultMarkup()
        {
            modelMarkup = new ModelMarkup();
            modelMarkup.IsVisibleDPTables = true;
            modelMarkup.PrevSizeDPTables = DefaultMarkup.HeightPanelTables;
            modelMarkup.CurrentSizeDPTables = DefaultMarkup.HeightPanelTables;
            
            return modelMarkup;
        }

        public void YamlSaveMarkup(ModelMarkup modelMarkup)
        {
            try
            {
                var serializer = new SerializerBuilder().Build();
                var yaml = serializer.Serialize(modelMarkup);

                using (StreamWriter sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "MarkupWnd.yaml", false, System.Text.Encoding.Default))
                {
                    sw.WriteLine(yaml);
                    sw.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }



    }
}
