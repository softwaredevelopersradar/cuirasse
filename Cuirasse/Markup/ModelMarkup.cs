﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Cuirasse
{
    class DefaultMarkup
    {
        public const double HeightPanelTables = 200;
    }

    public class ModelMarkup : INotifyPropertyChanged
    {
        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }
        #endregion

        public double PrevSizeDPTables { get; set; } = DefaultMarkup.HeightPanelTables;
        public double CurrentSizeDPTables { get; set; } = DefaultMarkup.HeightPanelTables;
        public bool IsVisibleDPTables { get; set; } = true;
        public double CurrentSizeProperties { get; set; }
        public double PrevSizeProperties { get; set; }
        public bool IsEnabledSplitter { get; set; }
        public bool IsCheckedBProperty { get; set; }
        

        public ModelMarkup()
        {

        }
    }
}
