﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Cuirasse
{
    public partial class MainWindow
    {
        private void BDefaultMarkup_Click(object sender, RoutedEventArgs e)
        {
            SetDefaultMurkup();
        }

        private void TBHideTables_Click(object sender, RoutedEventArgs e)
        {
            if (TBHideTables.IsChecked.Value)
            {
                modelMarkup.IsVisibleDPTables = true;
                modelMarkup.CurrentSizeDPTables = modelMarkup.PrevSizeDPTables;
            }
            else
            {
                modelMarkup.IsVisibleDPTables = false;
                modelMarkup.PrevSizeDPTables = modelMarkup.CurrentSizeDPTables;
                modelMarkup.CurrentSizeDPTables = 0;
            }

            YamlSaveMarkup(modelMarkup);
            ShowPanelTables();
        }

        private void GridSplitterMapTables_IsMouseCapturedChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            modelMarkup.PrevSizeDPTables = modelMarkup.CurrentSizeDPTables;
            modelMarkup.CurrentSizeDPTables = (double)GridTables.RowDefinitions[2].Height.Value;
            modelMarkup.IsVisibleDPTables = true;

            YamlSaveMarkup(modelMarkup);

        }

        private void gridSplitterV_IsMouseCapturedChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            modelMarkup.PrevSizeProperties = modelMarkup.CurrentSizeProperties;
            modelMarkup.CurrentSizeProperties = gridCentre.ColumnDefinitions[2].Width.Value;
            modelMarkup.IsEnabledSplitter = true;

            YamlSaveMarkup(modelMarkup);
        }
    }
}
