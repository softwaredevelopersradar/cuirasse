﻿using DllCuirassemProperties.Models;
using KirasaModelsDBLib;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using ValuesCorrectLib;
using YamlDotNet.Serialization;

namespace Cuirasse
{
    public partial class MainWindow
    {
        ModelMarkup modelMarkup;
        
        public void InitMarkup()
        { 
            modelMarkup = YamlLoadMarkup();

            ShowPanelTables();
            ClickBProperties();
            ShowProperties();
        }

        private void ShowPanelTables()
        {
            try
            {
                if (!modelMarkup.IsVisibleDPTables)
                {
                    GridTables.RowDefinitions[2].Height = new GridLength(0);
                    TBHideTables.IsChecked = false;
                }
                else
                {
                    GridTables.RowDefinitions[2].Height = new GridLength(modelMarkup.CurrentSizeDPTables);
                    TBHideTables.IsChecked = true;
                }
            }
            catch { }
        }

        private void ClickBProperties()
        {
            ucPanelButtons.bProperty.IsChecked = modelMarkup.IsCheckedBProperty;
        }

        private void ShowProperties()
        {
            try
            {
                if (!modelMarkup.IsCheckedBProperty)
                {
                    gridCentre.ColumnDefinitions[2].Width = new GridLength(0);
                    gridSplitterV.IsEnabled = false;
                }
                else
                {
                    gridCentre.ColumnDefinitions[2].Width = new GridLength(modelMarkup.CurrentSizeProperties);
                    gridSplitterV.IsEnabled = true;
                }
            }
            catch { }
        }

        public void SetDefaultMurkup()
        {
            modelMarkup.IsVisibleDPTables = true;
            modelMarkup.PrevSizeDPTables = DefaultMarkup.HeightPanelTables;
            modelMarkup.CurrentSizeDPTables = DefaultMarkup.HeightPanelTables;
            gridSplitterV.IsEnabled = true;
            modelMarkup.PrevSizeProperties = DefaultMarkup.HeightPanelTables;
            modelMarkup.CurrentSizeProperties = DefaultMarkup.HeightPanelTables;

            YamlSaveMarkup(modelMarkup);
            ShowPanelTables();
        }
    }
}