﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace Cuirasse
{
    [ValueConversion(sourceType: typeof(byte), targetType: typeof(Uri))]
    public class ModeLedConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            //try
            //{
            Uri uri;
            switch (System.Convert.ToByte(value))
            {
                
                case 4:
                    uri = new Uri(@"pack://application:,,,/"
                                + System.Reflection.Assembly.GetExecutingAssembly().GetName().Name
                                + ";component/"
                                + "Resources/LedBlue.png", UriKind.Absolute);
                    break;

                                    default:
                    uri = new Uri(@"pack://application:,,,/"
                                + System.Reflection.Assembly.GetExecutingAssembly().GetName().Name
                                + ";component/"
                                + "Resources/empty.png", UriKind.Absolute);
                    break;
            }

            return uri;
            //}

            //catch { return null; }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
