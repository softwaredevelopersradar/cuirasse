﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace Cuirasse
{
    [ValueConversion(sourceType: typeof(byte), targetType: typeof(string))]
    public class ModeLabelConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            //try
            //{
            string label;

            switch (System.Convert.ToByte(value))
            {
                case 2:
                    label = ValuesCorrectLib.SMeaning.meaningSearch; //"ПОИСК"
                    break;

                case 3:
                    label = ValuesCorrectLib.SMeaning.meaningLocation; // "МЕСТООПРЕДЕЛЕНИЕ"
                    break;

                default:
                    label = string.Empty;
                    break;
            }

            return label;
            //}

            //catch { return null; }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
