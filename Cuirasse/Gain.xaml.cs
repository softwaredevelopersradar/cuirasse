﻿using DAPprotocols;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Cuirasse
{
    /// <summary>
    /// Interaction logic for Gain.xaml
    /// </summary>
    public partial class Gain : Window
    {
        public Gain()
        {
            InitializeComponent();
            gSettings.Notify += GSettings_Notify;
        }

        public delegate void NeedGetRequestEvent(object sender, int EPO, double Gain, GainSettingsCuirasse.GSettings.Devices Devices);
        public event NeedGetRequestEvent NeedGetRequest;

        public delegate void NeedSetRequestEvent(object sender, int EPO, double Gain, GainSettingsCuirasse.GSettings.Devices Devices);
        public event NeedSetRequestEvent NeedSetRequest;

        private void GSettings_Notify(GainSettingsCuirasse.GSettings.Method Method, int EPO, double Gain, GainSettingsCuirasse.GSettings.Devices Devices)
        {
            switch (Method)
            {
                case GainSettingsCuirasse.GSettings.Method.Get:

                    NeedGetRequest?.Invoke(this, EPO, Gain, Devices);

                    break;

                case GainSettingsCuirasse.GSettings.Method.Set:

                    NeedSetRequest?.Invoke(this, EPO, Gain, Devices);

                    break;
            }
        }

        public void SetGainOnControl(DeviceGainMessage deviceGainMessage)
        {
            if (deviceGainMessage != null)
            {
                switch (deviceGainMessage.Device)
                {
                    case DapDevice.Preselector1:
                        gSettings.SetGainOnControl(deviceGainMessage.Gain, GainSettingsCuirasse.GSettings.Devices.Preselector1);
                        break;
                    case DapDevice.Preselector2:
                        gSettings.SetGainOnControl(deviceGainMessage.Gain, GainSettingsCuirasse.GSettings.Devices.Preselector2);
                        break;
                    case DapDevice.Preselector3:
                        gSettings.SetGainOnControl(deviceGainMessage.Gain, GainSettingsCuirasse.GSettings.Devices.Preselector3);
                        break;
                    case DapDevice.Preselector4:
                        gSettings.SetGainOnControl(deviceGainMessage.Gain, GainSettingsCuirasse.GSettings.Devices.Preselector4);
                        break;
                    case DapDevice.Receiver:
                        gSettings.SetGainOnControl(deviceGainMessage.Gain, GainSettingsCuirasse.GSettings.Devices.Receiver);
                        break;
                }
            }
        }

        public void SetEPO(int index)
        {
            //gSettings.CurrentEPOValue = index + 1;
            gSettings.SetCurrentEPOValue(index + 1);
        }


        protected override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
        {
            base.OnMouseLeftButtonDown(e);

            // Begin dragging the window
            this.DragMove();
        }

        public delegate void Event(object sender);
        public event Event NeedHide;

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                this.Hide();
                NeedHide?.Invoke(this);
            }
        }

        private void bClose_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();
            NeedHide?.Invoke(this);
        }

        public void SetLanguage(string param)
        {
            gSettings.ChangeLanguage(GainSettingsCuirasse.ApplicationViewModel.Languages.RU);
        }
    }
}
