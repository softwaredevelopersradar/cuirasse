﻿using System;
using System.ComponentModel;
using System.Windows;
using ValuesCorrectLib;

namespace Cuirasse
{
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        private void SetLanguageTables(DllCuirassemProperties.Models.Languages language)
        {
            ResourceDictionary dict = new ResourceDictionary();
            try
            {
                switch (language)
                {
                    case DllCuirassemProperties.Models.Languages.EN:
                        dict.Source = new Uri("/Cuirasse;component/Languages/TranslatorTables/TranslatorTablesCuirasse.EN.xaml",
                                      UriKind.Relative);
                        break;
                    case DllCuirassemProperties.Models.Languages.RU:
                        dict.Source = new Uri("/Cuirasse;component/Languages/TranslatorTables/TranslatorTablesCuirasse.RU.xaml",
                                           UriKind.Relative);
                        break;
                    default:
                        dict.Source = new Uri("/Cuirasse;component/Languages/TranslatorTables/TranslatorTablesCuirasse.RU.xaml",
                                          UriKind.Relative);
                        break;
                }

                this.Resources.MergedDictionaries.Add(dict);
            }
            catch (Exception)
            { }
        }

        private void basicProperties_OnLanguageChanged(object sender, DllCuirassemProperties.Models.Languages language)
        {
            SetLanguageTables(language);
            TranslatorTables.LoadDictionary(language);
            SetLanguagePanoramas(language);
            UpdatePropertyModel();
            ucOtherPoints.UpdateOtherPoints(lOtherPoints);
        }

    }
}
