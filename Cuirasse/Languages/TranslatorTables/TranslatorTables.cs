﻿using KirasaModelsDBLib;
using System;
using System.ComponentModel;
using System.Windows;

namespace Cuirasse
{
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        private void UcLocalPoints_OnIsWindowPropertyOpen(object sender, ReceivingPointControl.ReceivingPointProperty e)
        {
            e.SetLanguagePropertyGrid(basicProperties.Local.Common.Language);
        }

        private void UcFreqRangesRecon_OnIsWindowPropertyOpen(object sender, FreqRangesControl.FreqRangesProperty e)
        {
            e.SetLanguagePropertyGrid(basicProperties.Local.Common.Language);
        }

        private void UcFreqKnown_OnIsWindowPropertyOpen(object sender, FreqRangesControl.FreqRangesProperty e)
        {
            e.SetLanguagePropertyGrid(basicProperties.Local.Common.Language);
        }

        private void UcRemotePoints_OnIsWindowPropertyOpen(object sender, ReceivingPointControl.ReceivingPointProperty e)
        {
            e.SetLanguagePropertyGrid(basicProperties.Local.Common.Language);
        }

        private void UcOtherPoints_OnIsWindowPropertyOpen(object sender, OtherPointsControl.OtherPointsProperty e)
        {
            e.SetLanguagePropertyGrid(basicProperties.Local.Common.Language);
        }

        private void UcUAVResArchive_OnIsWindowPropertyOpen(object sender, UAVResArchiveControl.UAVResArchiveProperty e)
        {
            e.SetLanguagePropertyGrid(basicProperties.Local.Common.Language);
        }
    }
}
