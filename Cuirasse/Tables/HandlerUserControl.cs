﻿using KirasaModelsDBLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TableEvents;
using TableOperations;

namespace Cuirasse
{
    public partial class MainWindow
    {
        private void UcUAVRes_OnSaveTracks(object sender, EventArgs e)
        {
            SaveTrajectory.SaveTrajectoryUAVRes(lUAVRes, lUAVTrajectory);
        }

        private void UcUAVResArchive_OnSaveTracks(object sender, EventArgs e)
        {
            SaveTrajectory.SaveTrajectoryUAVResArchive(lUAVResArchive, lUAVTrajectoryArchive);
        }

        private void UcAeroscope_OnSaveTracks(object sender, EventArgs e)
        {
            SaveTrajectory.SaveTrajectoryAeroscope(lAeroscope, lATrajectory);
        }

        private void UcUAVResArchive_OnSelectedRow(object sender, SelectedRowEvents e)
        {
            if (e.Id > 0)
            {
                List<TableTrajectory> list = lUAVTrajectoryArchive.Where(x => x.TableUAVResId == PropNumUAV.SelectedIdUAVResArchive).ToList();
                ucUAVResArchive.UpdateUAVTrajectoryArchive(list);
            }
        }

        private void UcUAVResArchive_OnDisplayTrack(object sender, ListEvents e)
        {
            List<TableTrajectory> lUAVTArchive = new List<TableTrajectory>();

            for(int i = 0; i < e.Records.Count; i++)
            {
                List<TableTrajectory> list = lUAVTrajectoryArchive.Where(x => x.TableUAVResId == e.Records[i].Id).ToList();
                lUAVTArchive.AddRange(list);
            }

            ucMap.DisplayMapTrajectoriesArchive(e.Records, lUAVTArchive);
        }

        private void OnCentering(object sender, CenteringEvent e)
        {
            ucMap.CenteringMap(e.Coord);
        }
    }
}
