﻿using KirasaModelsDBLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TableEvents;

namespace Cuirasse
{
    public partial class MainWindow
    {
        public void InitTables()
        {
            #region Таблица Сигналы ИРИ
            ucSignalsUAV.OnDeleteRecord += new EventHandler<TableEvent>(OnDeleteRecord);
            ucSignalsUAV.OnChangeRecord += new EventHandler<TableEvent>(OnChangeRecord);
            ucSignalsUAV.OnClearRecords += new EventHandler<NameTable>(OnClearRecords);
            ucSignalsUAV.OnGetTypeM += new EventHandler<TableSignalsUAV>(UcSignalsUAV_OnGetTypeM);
            ucSignalsUAV.OnGetTypeL += new EventHandler<TableSignalsUAV>(UcSignalsUAV_OnGetTypeL);
            #endregion

            #region Таблица ИРИ БПЛА
            ucUAVResNew.OnDeleteRecord += new EventHandler<TableEvent>(OnDeleteRecord);
            ucUAVResNew.OnChangeRecord += new EventHandler<TableEvent>(OnChangeRecord);
            ucUAVResNew.OnClearRecords += new EventHandler<NameTable>(OnClearRecords);
            ucUAVResNew.OnSaveTracks += new EventHandler(UcUAVRes_OnSaveTracks);
            ucUAVResNew.OnArchiveRecord += new EventHandler<TableEvent>(OnArchiveRecord);
            ucUAVResNew.OnCentering += new EventHandler<CenteringEvent>(OnCentering);

            //ucUAVRes.OnDeleteRecord += new EventHandler<TableEvent>(OnDeleteRecord);
            //ucUAVRes.OnChangeRecord += new EventHandler<TableEvent>(OnChangeRecord);
            //ucUAVRes.OnClearRecords += new EventHandler<NameTable>(OnClearRecords);
            //ucUAVRes.OnSaveTracks += new EventHandler(UcUAVRes_OnSaveTracks);
            //ucUAVRes.OnArchiveRecord += new EventHandler<TableEvent>(OnArchiveRecord);
            //ucUAVRes.OnCentering += new EventHandler<CenteringEvent>(OnCentering);
            #endregion

            #region Таблица ИРИ БПЛА архив
            ucUAVResArchive.OnDeleteRecord += new EventHandler<TableEvent>(OnDeleteRecord);
            ucUAVResArchive.OnChangeRecord += new EventHandler<TableEvent>(OnChangeRecord);
            ucUAVResArchive.OnClearRecords += new EventHandler<NameTable>(OnClearRecords);
            ucUAVResArchive.OnSaveTracks += new EventHandler(UcUAVResArchive_OnSaveTracks);
            ucUAVResArchive.OnSelectedRow +=new EventHandler<SelectedRowEvents>(UcUAVResArchive_OnSelectedRow);
            ucUAVResArchive.OnIsWindowPropertyOpen += new EventHandler<UAVResArchiveControl.UAVResArchiveProperty>(UcUAVResArchive_OnIsWindowPropertyOpen);
            ucUAVResArchive.OnCombineTracks += new EventHandler<ListEvents>(OnCombineTracks);
            ucUAVResArchive.OnDisplayTrack += new EventHandler<ListEvents>(UcUAVResArchive_OnDisplayTrack);
            #endregion

            #region Таблица Локальные пункты приема (ЛПП)
            ucLocalPoints.OnAddRecord += new EventHandler<TableEvent>(OnAddRecord);
            ucLocalPoints.OnChangeRecord += new EventHandler<TableEvent>(OnChangeRecord);
            ucLocalPoints.OnDeleteRecord += new EventHandler<TableEvent>(OnDeleteRecord);
            ucLocalPoints.OnClearRecords += new EventHandler<NameTable>(OnClearRecords);
            ucLocalPoints.OnCentering += new EventHandler<CenteringEvent>(OnCentering);
            ucLocalPoints.OnIsWindowPropertyOpen += new EventHandler<ReceivingPointControl.ReceivingPointProperty>(UcLocalPoints_OnIsWindowPropertyOpen);
            #endregion

            #region Таблица Удаленные пункты приема (УПП)
            ucRemotePoints.OnAddRecord += new EventHandler<TableEvent>(OnAddRecord);
            ucRemotePoints.OnChangeRecord += new EventHandler<TableEvent>(OnChangeRecord);
            ucRemotePoints.OnDeleteRecord += new EventHandler<TableEvent>(OnDeleteRecord);
            ucRemotePoints.OnClearRecords += new EventHandler<NameTable>(OnClearRecords);
            ucRemotePoints.OnCentering += new EventHandler<CenteringEvent>(OnCentering);
            ucRemotePoints.OnIsWindowPropertyOpen += new EventHandler<ReceivingPointControl.ReceivingPointProperty>(UcRemotePoints_OnIsWindowPropertyOpen);
            #endregion

            #region Таблица Другие пункты приема (ДПП)
            ucOtherPoints.OnAddRecord += new EventHandler<TableEvent>(OnAddRecord);
            ucOtherPoints.OnChangeRecord += new EventHandler<TableEvent>(OnChangeRecord);
            ucOtherPoints.OnDeleteRecord += new EventHandler<TableEvent>(OnDeleteRecord);
            ucOtherPoints.OnClearRecords += new EventHandler<NameTable>(OnClearRecords);
          //  ucOtherPoints.OnCentering += new EventHandler<CenteringEvent>(OnCentering);
            ucOtherPoints.OnIsWindowPropertyOpen += new EventHandler<OtherPointsControl.OtherPointsProperty>(UcOtherPoints_OnIsWindowPropertyOpen);
            #endregion

            #region Таблица Известные частоты (ИЧ)
            ucFreqKnown.OnAddRecord += new EventHandler<TableEvent>(OnAddRecord);
            ucFreqKnown.OnChangeRecord += new EventHandler<TableEvent>(OnChangeRecord);
            ucFreqKnown.OnDeleteRecord += new EventHandler<TableEvent>(OnDeleteRecord);
            ucFreqKnown.OnClearRecords += new EventHandler<NameTable>(OnClearRecords);
            ucFreqKnown.OnIsWindowPropertyOpen += new EventHandler<FreqRangesControl.FreqRangesProperty>(UcFreqKnown_OnIsWindowPropertyOpen);
            #endregion

            #region Таблица Диапазоны радиоразведки (ДРР)
            ucFreqRangesRecon.OnAddRecord += new EventHandler<TableEvent>(OnAddRecord);
            ucFreqRangesRecon.OnChangeRecord += new EventHandler<TableEvent>(OnChangeRecord);
            ucFreqRangesRecon.OnDeleteRecord += new EventHandler<TableEvent>(OnDeleteRecord);
            ucFreqRangesRecon.OnClearRecords += new EventHandler<NameTable>(OnClearRecords);
            ucFreqRangesRecon.OnIsWindowPropertyOpen += new EventHandler<FreqRangesControl.FreqRangesProperty>(UcFreqRangesRecon_OnIsWindowPropertyOpen);
            #endregion

            #region Таблица Аэроскоп БПЛА (А БПЛА)
            ucAeroscope.OnDeleteRecord += new EventHandler<TableEvent>(OnDeleteRecord);
            ucAeroscope.OnChangeRecord += new EventHandler<TableEvent>(OnChangeRecord);
            ucAeroscope.OnChangeStatusAeroscope += UcAeroscope_OnChangeStatusAeroscope;
            ucAeroscope.OnClearRecords += new EventHandler<NameTable>(OnClearRecords);
            ucAeroscope.OnSaveTracks += new EventHandler(UcAeroscope_OnSaveTracks);
            #endregion

        }

        private void UcAeroscope_OnChangeStatusAeroscope(bool? isActive, TableAeroscope tableAeroscope)
        {
            if (isActive == null)
                return;

            if (isActive.Value)
            {
                this.SerialNumberToHideCollection.RemoveAll(x => x.Equals(tableAeroscope.SerialNumber));
            }
            else
            {
                this.SerialNumberToHideCollection.Add(tableAeroscope.SerialNumber);
            }
        }

        private void UcLocalPoints_OnCentering(object sender, CenteringEvent e)
        {
            throw new NotImplementedException();
        }

        private void UcUAVRes_OnCentering(object sender, CenteringEvent e)
        {
            throw new NotImplementedException();
        }
    }
}
