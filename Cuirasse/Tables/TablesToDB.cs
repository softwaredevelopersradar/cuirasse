﻿using KirasaModelsDBLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TableEvents;

namespace Cuirasse
{
    public partial class MainWindow
    {
        // Сигналы ИРИ 
        public List<TableSignalsUAV> lSignalsUAV = new List<TableSignalsUAV>();
        // ИРИ БПЛА
        public List<TableUAVBase> lUAVRes = new List<TableUAVBase>();
        // Траектория ИРИ БПЛА
        public List<TableTrajectory> lUAVTrajectory = new List<TableTrajectory>();
        // ИРИ БПЛА архив
        public List<TableUAVBase> lUAVResArchive = new List<TableUAVBase>();
        // Траектория ИРИ БПЛА архив
        public List<TableTrajectory> lUAVTrajectoryArchive = new List<TableTrajectory>();
        // Локальные пункты приема (ЛПП)
        public List<TableReceivingPoint> lLocalPoints = new List<TableReceivingPoint>();
        // Удаленные пункты приема(УПП)
        public List<TableReceivingPoint> lRemotePoints = new List<TableReceivingPoint>();
        // Другие пункты приема(ДПП)
        public List<TableOtherPoints> lOtherPoints = new List<TableOtherPoints>();
        // Известные частоты (ИЧ)
        public List<TableFreqRanges> lFreqKnown = new List<TableFreqRanges>();
        // Диапазоны радиоразведки (ДРР)
        public List<TableFreqRanges> lFreqRangesRecon = new List<TableFreqRanges>();
        // Аэроскоп БПЛА
        public List<TableAeroscope> lAeroscope = new List<TableAeroscope>();
        // Траектория А БПЛА
        public List<TableAeroscopeTrajectory> lATrajectory = new List<TableAeroscopeTrajectory>();

        public Dictionary<string, int> AeroscopeIds = new Dictionary<string, int>();

        public List<string> SerialNumberToHideCollection = new List<string>();

        // Для новой траектории в архиве
        private bool IsRemoveRange = false;
        public TableUAVBase NewTableUAVRes = new TableUAVBase();
        public List<TableUAVTrajectoryArchive> lNewTrack = new List<TableUAVTrajectoryArchive>();


        // Добавить запись
        private void OnAddRecord(object sender, TableEvent e)
        {
            if (wndViewModel.clientDB != null)
            {
                wndViewModel.clientDB.Tables[e.NameTable].Add(e.Record);
            }
        }

        // Удалить все записи
        private void OnClearRecords(object sender, NameTable nameTable)
        {
            if (wndViewModel.clientDB != null)
            {
                //wndViewModel.clientDB.Tables[nameTable].Clear();
                this.lAeroscope.Clear();
                this.lATrajectory.Clear();
                this.AeroscopeIds.Clear();
                ucAeroscope.ClearAeroscope();

                ucMap.UpdateMapTrajectories(this.lAeroscope, this.lATrajectory);
            }
        }

        // Удалить запись
        private void OnDeleteRecord(object sender, TableEvent e)
        {
            if (wndViewModel.clientDB != null)
            {
                //wndViewModel.clientDB.Tables[e.NameTable].Delete(e.Record);
            }
        }

        // Изменить запись
        private void OnChangeRecord(object sender, TableEvent e)
        {
            if (wndViewModel.clientDB != null)
            {
                if (e.NameTable == NameTable.TableАeroscope || e.NameTable == NameTable.TableАeroscopeTrajectory)
                {
                    return;
                }

                wndViewModel.clientDB.Tables[e.NameTable].Change(e.Record);
            }
        }

        private async void OnArchiveRecord(object sender, TableEvent e)
        {
            if (wndViewModel.clientDB != null)
            {
                lUAVResArchive = await wndViewModel.clientDB.Tables[NameTable.TableUAVResArchive].LoadAsync<TableUAVBase>();

                int maxId = 1;
                if(lUAVResArchive.Count != 0)
                {
                    maxId = lUAVResArchive.Max(x => x.Id);
                    if (maxId != -1) maxId = maxId + 1;
                }

                List<TableTrajectory> list = lUAVTrajectory.Where(x => x.TableUAVResId == e.Record.Id).ToList();
               
                // Источник
                e.Record.Id = maxId;
                wndViewModel.clientDB.Tables[e.NameTable].Add(e.Record);

                List<TableUAVTrajectoryArchive> lTArchive = new List<TableUAVTrajectoryArchive>();
                // Траектория
                for(int i = 0; i < list.Count; i++)
                {
                    list[i].Id = 0;
                    list[i].TableUAVResId = maxId;
                    lTArchive.Add(list[i].ToUAVTrajectoryArchive());
                }
                wndViewModel.clientDB.Tables[NameTable.TableUAVTrajectoryArchive].AddRange(lTArchive);
            }
        }

        private void OnCombineTracks(object sender, ListEvents e)
        {
            if (wndViewModel.clientDB != null)
            {
                List<TableTrajectory> lCombineTracks = new List<TableTrajectory>();
                for (int i = 0; i < e.Records.Count; i++)
                {
                    List<TableTrajectory> lTemp = lUAVTrajectoryArchive.Where(x => x.TableUAVResId == e.Records[i].Id).ToList();
                    lCombineTracks.AddRange(lTemp);
                }

                List<TableTrajectory> lTimeSort = ucUAVResArchive.CombineTracksArchive(lCombineTracks);

                List<TableUAVResArchive> list = new List<TableUAVResArchive>();
                for (int i = 0; i < e.Records.Count; i++)
                {
                    list.Add(e.Records[i].ToUAVResArchive());
                }

                // Новый источник
                NewTableUAVRes = new TableUAVBase();
                NewTableUAVRes = e.Records[0];
                NewTableUAVRes.IsActive = false;
                NewTableUAVRes.Type = e.TypeUAVRes;
                NewTableUAVRes.TrackTime = ucUAVResArchive.UpdateTrackTimeUAVResArchive(lTimeSort);

                // Новая траектория
                lNewTrack = new List<TableUAVTrajectoryArchive>();
                for (int i = 0; i < lTimeSort.Count; i++)
                {
                    lTimeSort[i].Id = 0;
                    lTimeSort[i].TableUAVResId = e.Records[0].Id;
                    lTimeSort[i].Num = (short)(i + 1);
                    lNewTrack.Add(lTimeSort[i].ToUAVTrajectoryArchive());
                }

                // Удалить выбранные (для объединения)
                IsRemoveRange = true;
                wndViewModel.clientDB.Tables[e.NameTable].RemoveRange(list);
                
            }
        }
    }
}
