﻿using DAPprotocols;
using KirasaModelsDBLib;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Cuirasse
{
    public partial class MainWindow
    {
        TechWindow techWindow = new TechWindow();

        private void InitTechWindow()
        {
            techWindow.NeedHide += TechWindow_NeedHide;
        }

        private void TechWindow_NeedHide(object sender)
        {
            TechToggleButton.IsChecked = false;
        }

        private void StartRecordButton_OnClick(object sender, RoutedEventArgs e)
        {
            this.client.SendExtraMessage(ExtraCode.WriteCommand);
        }

        private void TechToggleButton_Click(object sender, RoutedEventArgs e)
        {
            if (TechToggleButton.IsChecked.Value)
                techWindow.Show();
            else
                techWindow.Hide();

            techWindow.Properties.DoSmth();
            //techWindow.Properties.UpdateMS(10, 20, 30);
            //techWindow.Properties.UpdateAero(40, 50, 60);
            //techWindow.Properties.UpdateRDM(70, 80, 90);
            //techWindow.Properties.UpdateRDMTraj(100, 110, 120);
        }

        public void SendTableStationToTechWindow(List<TableReceivingPoint> tableReceivingPoints)
        {
            TableReceivingPoint MainStation = tableReceivingPoints.Where(x => x.IsCetral == true).FirstOrDefault();
            if (MainStation != null)
            {
                techWindow.Properties.UpdateMS(
                    MainStation.Coordinates.Latitude, 
                    MainStation.Coordinates.Longitude,
                    MainStation.Coordinates.Altitude);
            }
        }

        public void SendTableGlobalPropertiesToTechWindow(DllCuirassemProperties.Models.GlobalProperties arg)
        {
            techWindow.Properties.UpdateOP(arg.Common.OperatorLocationLatitude, arg.Common.OperatorLocationLongitude, 0);
        }

        public void SendTableGlobalPropertiesToTechWindow(GlobalProperties arg)
        {
            techWindow.Properties.UpdateOP(arg.Common.OperatorLatitude, arg.Common.OperatorLongitude, 0);
        }

        public void SendAeroTableToTechWindow(List<TableAeroscope> tableAeroscopes , List<TableAeroscopeTrajectory> tableAeroscopeTrajectories)
        {
            var FirstChekedAero = tableAeroscopes.Where(x => x.IsActive).FirstOrDefault();

            if (FirstChekedAero != null)
            {
                var ArrayPointsAeroTraj = tableAeroscopeTrajectories.Where(x => x.SerialNumber == FirstChekedAero.SerialNumber).ToList();
                var ArrayPointsAeroTraj2 = tableAeroscopeTrajectories.Where(x => x.Id == FirstChekedAero.Id).ToList();

                var LastPointAeroTraj = ArrayPointsAeroTraj.OrderBy(x => x.Num).LastOrDefault();
                var LastPointAeroTraj2 = ArrayPointsAeroTraj2.OrderBy(x => x.Num).LastOrDefault();

                if (LastPointAeroTraj != null)
                {
                    techWindow.Properties.UpdateAero(
                        LastPointAeroTraj.Coordinates.Latitude,
                        LastPointAeroTraj.Coordinates.Longitude,
                        LastPointAeroTraj.Coordinates.Altitude);
                }
            }
        }

        public void SendRDMToTechWindow(CoordsMessage coordsMessage)
        {
            techWindow.Properties.UpdateRDM(
                coordsMessage.Lat,
                coordsMessage.Lon,
                (float)coordsMessage.Alt);
        }

        public void SendRDMTrajTableToTechWindow(List<TableUAVBase> tableUAVBases, List<TableTrajectory> tableTrajectories)
        {
            var FirstChekedRDMTraj = tableUAVBases.Where(x => x.IsActive).FirstOrDefault();

            if (FirstChekedRDMTraj != null)
            {
                var ArrayPointsRDMTraj = tableTrajectories.Where(x => x.TableUAVResId == FirstChekedRDMTraj.Id).ToList();
                var ArrayPointsRDMTraj2 = tableTrajectories.Where(x => x.Id == FirstChekedRDMTraj.Id).ToList();

                var LastPointRDMTraj = ArrayPointsRDMTraj.OrderBy(x => x.Num).LastOrDefault();
                var LastPointAeroTraj2 = ArrayPointsRDMTraj2.OrderBy(x => x.Num).LastOrDefault();

                if (LastPointRDMTraj != null)
                {
                    techWindow.Properties.UpdateRDMTraj(
                        LastPointRDMTraj.Coordinates.Latitude,
                        LastPointRDMTraj.Coordinates.Longitude,
                        LastPointRDMTraj.Coordinates.Altitude);
                }
            }
        }

        public void ChangeTechWindowLaguage(DllCuirassemProperties.Models.Languages language)
        {
            string shortLang = language.ToString().ToLower();

            var defaultLang = TestCuirasse.TestCuirasseProperties.Languages.RU;

            if (shortLang.Contains("ru")) defaultLang = TestCuirasse.TestCuirasseProperties.Languages.RU;
            if (shortLang.Contains("en")) defaultLang = TestCuirasse.TestCuirasseProperties.Languages.EN;

            techWindow.ChangeLanguage(defaultLang);
        }
    }
}
