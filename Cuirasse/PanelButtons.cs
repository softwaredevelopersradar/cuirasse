﻿using PanelButtonsControl;
using System.Windows;
using DAPServerClient2;
using System;

namespace Cuirasse
{
    public partial class MainWindow : Window
    {
        private async void ucPanelButtons_OnButtonClick(object sender, ButtonClick e)
        {
            switch (e.ButtonMode)
            {
                case ButtonMode.Property:
                    if (e.IsButtonChecked)
                    {
                        modelMarkup.CurrentSizeProperties = modelMarkup.PrevSizeProperties;
                        modelMarkup.IsCheckedBProperty = true;
                    }
                    else
                    {
                        if(modelMarkup.CurrentSizeProperties != 0)
                            modelMarkup.PrevSizeProperties = modelMarkup.CurrentSizeProperties;
                        modelMarkup.CurrentSizeProperties = 0;
                        modelMarkup.IsCheckedBProperty = false;
                    }
                    YamlSaveMarkup(modelMarkup);
                    ShowProperties();

                    break;

                case ButtonMode.Stop:
                    // TEST -------------------------------------------
                    //wndViewModel.PropertyModel.ModeLed = 4;
                    //wndViewModel.PropertyModel.ModeLabel = 2;
                    // ------------------------------------------- TEST
                    wndViewModel.PropertyModel.ModeLed = 0;
                    wndViewModel.PropertyModel.ModeLabel = 0;

                    wndViewModel.PropertyModel.ModeLabelCurrent = wndViewModel.PropertyModel.ModeLabel;
                    var answer0 = await client.SetMode(DAPprotocols.DapServerMode.Stop);
                    PanoramaWindow.pLibrary.Mode = 0;
                    PanoramaWindow.Panoramas.UpdateMode(0);

                    if(wndViewModel.LocalPropertiesModel.Common.Access == DllCuirassemProperties.Models.AccessTypes.Admin)
                        ucMap.ClearListsTest();
                    break;

                case ButtonMode.Start:
                    // TEST -------------------------------------------
                    //wndViewModel.PropertyModel.ModeLed = 0;
                    //wndViewModel.PropertyModel.ModeLabel = 3;
                    // ------------------------------------------- TEST
                    wndViewModel.PropertyModel.ModeLabelCurrent = wndViewModel.PropertyModel.ModeLabel;
                    var answer1 = await client.SetMode(DAPprotocols.DapServerMode.RadioIntelligence);                    
                    PanoramaWindow.pLibrary.Mode = 1;
                    PanoramaWindow.Panoramas.UpdateMode(1);
                    break;
            }
        }
    }
}
