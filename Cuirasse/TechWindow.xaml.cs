﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Cuirasse
{
    /// <summary>
    /// Interaction logic for TechWindow.xaml
    /// </summary>
    public partial class TechWindow : Window
    {
        public TechWindow()
        {
            InitializeComponent();
            Properties.ChangeLanguage(TestCuirasse.TestCuirasseProperties.Languages.RU);
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            e.Cancel = true;
            this.Hide();
            NeedHide?.Invoke(this);
        }

        public void ChangeLanguage(TestCuirasse.TestCuirasseProperties.Languages languages)
        {
            Properties.ChangeLanguage(languages);
        }

        public delegate void Event(object sender);
        public event Event NeedHide;

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                this.Hide();
                NeedHide?.Invoke(this);
            }
        }

        private void bClose_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();
            NeedHide?.Invoke(this);
        }

    }
}
