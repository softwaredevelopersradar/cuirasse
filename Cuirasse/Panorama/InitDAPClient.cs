﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using DAPprotocols;
using DAPServerClient2;
using DllCuirassemProperties.Models;
using EScopeProcessor;
using KirasaModelsDBLib;
using UDPASReceiver;

namespace Cuirasse
{
    public partial class MainWindow
    {
        private UDPASReceiver.Client asClient;
        private EScopeMain esClient;

        private void InitDAPClientEvents()
        {
            client.IsConnected += Client_IsConnected;
            client.IsRead += Client_IsRead;
            client.IsWrite += Client_IsWrite;

            client.GetCorrFuncUpdate += Client_GetCorrFuncUpdate;
            client.GetTausMessageUpdate += Client_GetTausMessageUpdate;
            client.GetCoordsMessageUpdate += Client_GetCoordsMessageUpdate;
            client.GetExtraordinaryModeMessageUpdate += Client_GetExtraordinaryModeMessageUpdate;
        }

        private void InitPLibrary()
        {
            DispatchIfNecessary(() => 
            {
                PanoramaWindow.pLibrary.ViewMode = 1;
                PanoramaWindow.pLibrary.GlobalRangeXmin = 10;
                PanoramaWindow.pLibrary.GlobalBandWidthMHz = 62.5;
                PanoramaWindow.pLibrary.GlobalNumberOfBands = 96;
                PanoramaWindow.pLibrary.Seconds = 3600;
                PanoramaWindow.pLibrary.GlobalRangeYmin = -120;
                PanoramaWindow.pLibrary.GlobalRangeYmax = 0;
            });
        }
        private void InitPLibrary2()
        {
            DispatchIfNecessary(() =>
            {
                PanoramaWindow.pLibrary.ViewMode = 1;
                PanoramaWindow.pLibrary.GlobalRangeXmin = 10;
                PanoramaWindow.pLibrary.GlobalBandWidthMHz = 125;
                PanoramaWindow.pLibrary.GlobalNumberOfBands = 48;
                PanoramaWindow.pLibrary.Seconds = 3600;
                PanoramaWindow.pLibrary.GlobalRangeYmin = -120;
                PanoramaWindow.pLibrary.GlobalRangeYmax = 0;
            });
        }

        private void InitPLIbraryEvents()
        {
            PanoramaWindow.pLibrary.NeedSpectrumRequest += PLibrary_NeedSpectrumRequest;
            PanoramaWindow.pLibrary.ThresholdChange += PLibrary_ThresholdChangeAsync;
            PanoramaWindow.pLibrary.NeedExBearingRequest += PLibrary_NeedExBearingRequest;
            PanoramaWindow.pLibrary.OnGetRecordValue += PLibrary_OnGetRecordValue;
            PanoramaWindow.pLibrary.OnGetRecognValue += PLibrary_OnGetRecognValue;
            PanoramaWindow.pLibrary.ApplyFilterValue += PLibrary_ApplyFilterValue;
            PanoramaWindow.pLibrary.OnSendCBIndex += PLibrary_OnSendCBIndex;
            PanoramaWindow.pLibrary.OnSendCBCorrIndex += PLibrary_OnSendCBCorrIndex;
            PanoramaWindow.pLibrary.OnCorrelationVisibility += PLibrary_OnCorrelationVisibility;
            PanoramaWindow.pLibrary.OnSetFreqToPanoram += PLibrary_OnSetFreqToPanoram;
            PanoramaWindow.pLibrary.NbarIndex += PLibrary_NbarIndex;
            PanoramaWindow.pLibrary.NbarMiddleIndex += PLibrary_NbarMiddleIndex;
            PanoramaWindow.pLibrary.OnChannelStatus += PLibrary_OnChannelStatus;

            PanoramaWindow.pLibrary.OnFreqArea += PLibrary_OnFreqArea;
        }

        private void AsClient_OnUDPPacketReceived(object sender, MyEventArgsReadyData e)
        {
            if (e is null)
                return;

            TableAeroscope tableAeroscope = new TableAeroscope()
            {
                Id = this.GetIdAeroscopeBySerialNumber(e.SerialNumber),
                HomeLatitude = e.HomeLatitude,
                HomeLongitude = e.HomeLongitude,
                SerialNumber = e.SerialNumber,
                UUID = e.UUID,
                UUIDLength = e.UUIDLength,
                Type = e.Type,
                IsActive = true,
            };

            TableAeroscopeTrajectory tableTrajectory = new TableAeroscopeTrajectory()
            {
                Id = this.GetIdAeroscopeBySerialNumber(e.SerialNumber),
                Coordinates = new Coord()
                {
                    Latitude = e.Latitude,
                    Longitude = e.Longitude,
                    Altitude = e.Altitude
                },
                SerialNumber = e.SerialNumber,
                Pitch = e.Pitch,
                Elevation = e.Elevation,
                Roll = e.Roll,
                Time = e.Time,
                V_east = e.V_east,
                V_up = e.V_up,
                V_north = e.V_north,
                Yaw = e.Yaw
            };

            this.UpdateAerscopeInformation(tableAeroscope, tableTrajectory);
        }

        private int GetIdAeroscopeBySerialNumber(string serialNumber)
        {
            if (this.AeroscopeIds is null)
                return 0;


            if (this.AeroscopeIds.ContainsKey(serialNumber))
                return this.AeroscopeIds[serialNumber];

            int id = 0;
            var valuesCollection = this.AeroscopeIds.Values;
            if (valuesCollection.Count != 0)
            {
                id = valuesCollection.Max() + 1;
            }

            this.AeroscopeIds.Add(serialNumber, id);

            return id;
        }

        private bool IsAddAeroscopeNeed(string serialNumber) => this.AeroscopeIds.ContainsKey(serialNumber);

        private void EsMainClient_EScopeProcessor_OnParseFinishedSuccessfully(EScopeDomain.Models.ScopeParseResult parseResult)
        {
            if (parseResult is null)
                return;

            TableAeroscope tableAeroscope = new TableAeroscope()
            {
                Id = this.GetIdAeroscopeBySerialNumber(parseResult.SerialNumber),
                HomeLatitude = parseResult.HomePointCoordinates.Latitude,
                HomeLongitude = parseResult.HomePointCoordinates.Longitude,
                SerialNumber = parseResult.SerialNumber,
                IsActive = true,
                //UUID = scopeResult.UUID,
                //UUIDLength = scopeResult.UUIDLength,
                Type = string.Concat("ES_", parseResult.UAVType.ToString())
            };

            TableAeroscopeTrajectory tableTrajectory = new TableAeroscopeTrajectory()
            {
                Id = this.GetIdAeroscopeBySerialNumber(parseResult.SerialNumber),
                Coordinates = new Coord()
                {
                    Latitude = parseResult.UAVCoordinates.Latitude,
                    Longitude = parseResult.UAVCoordinates.Longitude,
                    Altitude = (float)parseResult.UAVCoordinates.Altitude
                },
                SerialNumber = parseResult.SerialNumber,
                //Pitch = scopeResult.Pitch,
                Elevation = (float)parseResult.UAVCoordinates.Altitude,
                //Roll = scopeResult.Roll,
                Time = DateTime.Now,
                //V_east = scopeResult.V_east,
                //V_up = scopeResult.V_up,
                //V_north = scopeResult.V_north,
                //Yaw = scopeResult.Yaw
            };

            this.UpdateAerscopeInformation(tableAeroscope, tableTrajectory);
        }

        /*private void EsClient_OnObtainResult(EScopeLib.Models.ScopeResult scopeResult)
        {
            if (scopeResult is null)
                return;

            TableAeroscope tableAeroscope = new TableAeroscope()
            {
                Id = this.GetIdAeroscopeBySerialNumber(scopeResult.SerialNumber),
                HomeLatitude = scopeResult.HomePointCoordinates.Latitude,
                HomeLongitude = scopeResult.HomePointCoordinates.Longitude,
                SerialNumber = scopeResult.SerialNumber,
                IsActive = true,
                //UUID = scopeResult.UUID,
                //UUIDLength = scopeResult.UUIDLength,
                Type = string.Concat("ES_", scopeResult.UAVType.ToString())
            };

            TableAeroscopeTrajectory tableTrajectory = new TableAeroscopeTrajectory()
            {
                Id = this.GetIdAeroscopeBySerialNumber(scopeResult.SerialNumber),
                Coordinates = new Coord()
                {
                    Latitude = scopeResult.UAVCoordinates.Latitude,
                    Longitude = scopeResult.UAVCoordinates.Longitude,
                    Altitude = (float)scopeResult.UAVCoordinates.Altitude
                },
                SerialNumber = scopeResult.SerialNumber,
                //Pitch = scopeResult.Pitch,
                Elevation = (float)scopeResult.UAVCoordinates.Altitude,
                //Roll = scopeResult.Roll,
                Time = DateTime.Now,
                //V_east = scopeResult.V_east,
                //V_up = scopeResult.V_up,
                //V_north = scopeResult.V_north,
                //Yaw = scopeResult.Yaw
            };

            this.UpdateAerscopeInformation(tableAeroscope, tableTrajectory);
        }*/

        private void UpdateAerscopeInformation(TableAeroscope tableAeroscope, TableAeroscopeTrajectory tableAeroscopeTrajectory)
        {
            if (this.AeroscopeIds == null)
                return;

            if (this.IsAddAeroscopeNeed(tableAeroscope.SerialNumber))
            {
                if (!this.lAeroscope.Any(x => x.SerialNumber.Equals(tableAeroscope.SerialNumber)))
                {
                    this.lAeroscope.Add(tableAeroscope);
                }
                else
                {
                    var aeroscopeRecordIndex = this.lAeroscope.FindIndex(x => x.SerialNumber.Equals(tableAeroscope.SerialNumber));

                    if (aeroscopeRecordIndex != -1)
                    {
                        this.lAeroscope[aeroscopeRecordIndex].HomeLatitude = tableAeroscope.HomeLatitude;
                        this.lAeroscope[aeroscopeRecordIndex].HomeLongitude = tableAeroscope.HomeLongitude;
                        this.lAeroscope[aeroscopeRecordIndex].Type = tableAeroscope.Type;
                        this.lAeroscope[aeroscopeRecordIndex].UUID = tableAeroscope.UUID;
                        this.lAeroscope[aeroscopeRecordIndex].UUIDLength = tableAeroscope.UUIDLength;
                    }
                }

                this.lATrajectory.Add(tableAeroscopeTrajectory);
            }

            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {

                if (lAeroscope.Count == 0)
                {
                    ucAeroscope.UpdateAeroscope(lAeroscope);
                }
                else
                {
                    ucAeroscope.AddAeroscope(lAeroscope);
                }

                if (this.lATrajectory.Count != 0)
                {
                    this.ucAeroscope.AddATrajectory(lATrajectory);
                }

                ucMap.UpdateMapTrajectories(
                    lAeroscope.Where(x => !this.SerialNumberToHideCollection.Contains(x.SerialNumber)).ToList(), 
                    lATrajectory.Where(x => !this.SerialNumberToHideCollection.Contains(x.SerialNumber)).ToList()
                );

                //SendAeroTableToTechWindow(lAeroscope, lATrajectory);
            });
        }

        private void PLibrary_OnChannelStatus(object sender, PanoramaLibrary.ChannelsStatusEventArgs e)
        {
            wndViewModel.LocalPropertiesModel.Spectrum.Chanel_1 = e.Channel1;
            wndViewModel.LocalPropertiesModel.Spectrum.Chanel_2 = e.Channel2;
            wndViewModel.LocalPropertiesModel.Spectrum.Chanel_3 = e.Channel3;
            wndViewModel.LocalPropertiesModel.Spectrum.Chanel_4 = e.Channel4;
            wndViewModel.LocalPropertiesModel.Spectrum.Chanel_Median = e.Md;
            wndViewModel.LocalPropertiesModel.Spectrum.Chanel_Max = e.Mx;
            wndViewModel.LocalPropertiesModel.Spectrum.Chanel_Sum = e.Sm;
                        
            YamlSave(wndViewModel.LocalPropertiesModel);
        }

        private void PLibrary_NbarIndex(object sender, int index)
        {
            SetEPO(index); 
            
            // old middle button action

            OnClearRecords(this, NameTable.TableFreqRangesRecon);

            double FreqMinKHz = (PanoramaWindow.pLibrary.GlobalRangeXmin + index * PanoramaWindow.pLibrary.GlobalBandWidthMHz) * 1000;
            double FreqMaxKHz = (PanoramaWindow.pLibrary.GlobalRangeXmin + (index + 1) * PanoramaWindow.pLibrary.GlobalBandWidthMHz) * 1000;

            TableFreqRangesRecon tableFreqRangesRecon = new TableFreqRangesRecon()
            {
                Id = 0,
                FreqMinKHz = FreqMinKHz,
                FreqMaxKHz = FreqMaxKHz,
                IsActive = true
            };

            OnAddRecord(this, new TableEvents.TableEvent(tableFreqRangesRecon));
        }

        private void PLibrary_NbarMiddleIndex(object sender, int index)
        {
            
        }

        private async void PLibrary_ApplyFilterValue(object sender, bool value)
        {
            var answer = await client.ApplyExFilter(value);
        }

        private async void PLibrary_OnGetRecordValue(bool value)
        {
            //ЗАПИСЬ СПЕКТРА
            var answer = await client.ToggleStartStop(value, ToggleMode.Record);
        }

        private async void PLibrary_OnGetRecognValue(bool value)
        {
            var answer = await client.ToggleStartStop(value, ToggleMode.Recognition);
        }

        private async void PLibrary_NeedExBearingRequest(object sender, double MinBandX, double MaxBandX, int PhAvCount, int PlAvCount)
        {
            //запрос коор функции
            var answer = await client.GetCorrFunc(0, MinBandX, MaxBandX);
            //var answer1 = await client.GetCorrFunc(1, MinBandX, MaxBandX);
        }

        private async void PLibrary_NeedSpectrumRequest(object sender, double MinVisibleX, double MaxVisibleX, int PointCount, byte id)
        {
            //запрос спектра от сервера
            if (IsPanoramaHide)
            {
                var answer = new GetSpectrumResponse();
                switch (id)
                {
                    case 1: answer = await client.GetSpectrum(0, ChannelPicker.Channel1, MinVisibleX, MaxVisibleX, PointCount); break;
                    case 2: answer = await client.GetSpectrum(0, ChannelPicker.Channel2, MinVisibleX, MaxVisibleX, PointCount); break;
                    case 3: answer = await client.GetSpectrum(0, ChannelPicker.Channel3, MinVisibleX, MaxVisibleX, PointCount); break;
                    case 4: answer = await client.GetSpectrum(0, ChannelPicker.Channel4, MinVisibleX, MaxVisibleX, PointCount); break;
                    case 5: answer = await client.GetSpectrum(0, ChannelPicker.ChannelMax, MinVisibleX, MaxVisibleX, PointCount); break;
                    case 6: answer = await client.GetSpectrum(0, ChannelPicker.ChannelMedian, MinVisibleX, MaxVisibleX, PointCount); break;
                    case 7: answer = await client.GetSpectrum(0, ChannelPicker.ChannelSum, MinVisibleX, MaxVisibleX, PointCount); break;
                    default: break;
                }

                AnswerGetSpectrum(answer);
            }
        }

        private async void PLibrary_ThresholdChangeAsync(object sender, int value)
        {
            //отправить на сервер
            var answer = await client.SetFilters((short)value, ThresholdType.RIThreshold);
            //или передать куда ещё, а потом на сервер
        }

        private async void PanoramasGraphs_NeedSpectrumRequest5(object sender, byte ID, double MinVisibleX, double MaxVisibleX, int PointCount)
        {
            if (IsPanoramaHide)
            {
                var answer = new GetSpectrumResponse();

                switch (ID)
                {
                    case 1: answer = await client.GetSpectrum(5, ChannelPicker.Channel1, MinVisibleX, MaxVisibleX, PointCount); break;
                    case 2: answer = await client.GetSpectrum(5, ChannelPicker.Channel2, MinVisibleX, MaxVisibleX, PointCount); break;
                    case 3: answer = await client.GetSpectrum(5, ChannelPicker.Channel3, MinVisibleX, MaxVisibleX, PointCount); break;
                    case 4: answer = await client.GetSpectrum(5, ChannelPicker.Channel4, MinVisibleX, MaxVisibleX, PointCount); break;
                    case 5: answer = await client.GetSpectrum(5, ChannelPicker.ChannelMax, MinVisibleX, MaxVisibleX, PointCount); break;
                    case 6: answer = await client.GetSpectrum(5, ChannelPicker.ChannelMedian, MinVisibleX, MaxVisibleX, PointCount); break;
                    case 7: answer = await client.GetSpectrum(5, ChannelPicker.ChannelSum, MinVisibleX, MaxVisibleX, PointCount); break;
                    default: break;
                }

                AnswerGetSpectrum(answer);
            }
        }

        private async void PanoramasGraphs_NeedSpectrumRequest4(object sender, byte ID, double MinVisibleX, double MaxVisibleX, int PointCount)
        {
            if (IsPanoramaHide)
            {
                var answer = new GetSpectrumResponse();

                switch (ID)
                {
                    case 1: answer = await client.GetSpectrum(4, ChannelPicker.Channel1, MinVisibleX, MaxVisibleX, PointCount); break;
                    case 2: answer = await client.GetSpectrum(4, ChannelPicker.Channel2, MinVisibleX, MaxVisibleX, PointCount); break;
                    case 3: answer = await client.GetSpectrum(4, ChannelPicker.Channel3, MinVisibleX, MaxVisibleX, PointCount); break;
                    case 4: answer = await client.GetSpectrum(4, ChannelPicker.Channel4, MinVisibleX, MaxVisibleX, PointCount); break;
                    case 5: answer = await client.GetSpectrum(4, ChannelPicker.ChannelMax, MinVisibleX, MaxVisibleX, PointCount); break;
                    case 6: answer = await client.GetSpectrum(4, ChannelPicker.ChannelMedian, MinVisibleX, MaxVisibleX, PointCount); break;
                    case 7: answer = await client.GetSpectrum(4, ChannelPicker.ChannelSum, MinVisibleX, MaxVisibleX, PointCount); break;
                    default: break;
                }

                AnswerGetSpectrum(answer);
            }
        }

        private async void PanoramasGraphs_NeedSpectrumRequest3(object sender, byte ID, double MinVisibleX, double MaxVisibleX, int PointCount)
        {
            if (IsPanoramaHide)
            {
                var answer = new GetSpectrumResponse();

                switch (ID)
                {
                    case 1: answer = await client.GetSpectrum(3, ChannelPicker.Channel1, MinVisibleX, MaxVisibleX, PointCount); break;
                    case 2: answer = await client.GetSpectrum(3, ChannelPicker.Channel2, MinVisibleX, MaxVisibleX, PointCount); break;
                    case 3: answer = await client.GetSpectrum(3, ChannelPicker.Channel3, MinVisibleX, MaxVisibleX, PointCount); break;
                    case 4: answer = await client.GetSpectrum(3, ChannelPicker.Channel4, MinVisibleX, MaxVisibleX, PointCount); break;
                    case 5: answer = await client.GetSpectrum(3, ChannelPicker.ChannelMax, MinVisibleX, MaxVisibleX, PointCount); break;
                    case 6: answer = await client.GetSpectrum(3, ChannelPicker.ChannelMedian, MinVisibleX, MaxVisibleX, PointCount); break;
                    case 7: answer = await client.GetSpectrum(3, ChannelPicker.ChannelSum, MinVisibleX, MaxVisibleX, PointCount); break;
                    default: break;
                }

                AnswerGetSpectrum(answer);
            }
        }

        private async void PanoramasGraphs_NeedSpectrumRequest2(object sender, byte ID, double MinVisibleX, double MaxVisibleX, int PointCount)
        {
            if (IsPanoramaHide)
            {
                var answer = new GetSpectrumResponse();

                switch (ID)
                {
                    case 1: answer = await client.GetSpectrum(2, ChannelPicker.Channel1, MinVisibleX, MaxVisibleX, PointCount); break;
                    case 2: answer = await client.GetSpectrum(2, ChannelPicker.Channel2, MinVisibleX, MaxVisibleX, PointCount); break;
                    case 3: answer = await client.GetSpectrum(2, ChannelPicker.Channel3, MinVisibleX, MaxVisibleX, PointCount); break;
                    case 4: answer = await client.GetSpectrum(2, ChannelPicker.Channel4, MinVisibleX, MaxVisibleX, PointCount); break;
                    case 5: answer = await client.GetSpectrum(2, ChannelPicker.ChannelMax, MinVisibleX, MaxVisibleX, PointCount); break;
                    case 6: answer = await client.GetSpectrum(2, ChannelPicker.ChannelMedian, MinVisibleX, MaxVisibleX, PointCount); break;
                    case 7: answer = await client.GetSpectrum(2, ChannelPicker.ChannelSum, MinVisibleX, MaxVisibleX, PointCount); break;
                    default: break;
                }

                AnswerGetSpectrum(answer);
            }
        }

        private async void PanoramasGraphs_NeedSpectrumRequest(object sender, byte ID, double MinVisibleX, double MaxVisibleX, int PointCount)
        {
            if (IsPanoramaHide)
            {

                var answer = new GetSpectrumResponse();

                switch (ID)
                {
                    case 1: answer = await client.GetSpectrum(1, ChannelPicker.Channel1, MinVisibleX, MaxVisibleX, PointCount); break;
                    case 2: answer = await client.GetSpectrum(1, ChannelPicker.Channel2, MinVisibleX, MaxVisibleX, PointCount); break;
                    case 3: answer = await client.GetSpectrum(1, ChannelPicker.Channel3, MinVisibleX, MaxVisibleX, PointCount); break;
                    case 4: answer = await client.GetSpectrum(1, ChannelPicker.Channel4, MinVisibleX, MaxVisibleX, PointCount); break;
                    case 5: answer = await client.GetSpectrum(1, ChannelPicker.ChannelMax, MinVisibleX, MaxVisibleX, PointCount); break;
                    case 6: answer = await client.GetSpectrum(1, ChannelPicker.ChannelMedian, MinVisibleX, MaxVisibleX, PointCount); break;
                    case 7: answer = await client.GetSpectrum(1, ChannelPicker.ChannelSum, MinVisibleX, MaxVisibleX, PointCount); break;
                    default: break;
                }

                AnswerGetSpectrum(answer);
            }
        }

        private async void Panoramas_OnFreqArea(PanoramasControl.PanoramasGraph.FrequencyType frequencyType, double startFreq, double endFreq, int idPanoram)
        {
            switch (idPanoram)
            {
                case 1:
                    switch (frequencyType)
                    {
                        case PanoramasControl.PanoramasGraph.FrequencyType.Signal:
                            var answer = await client.SendRangeMessage(startFreq, endFreq, RangeType.Signal);
                            break;
                        case PanoramasControl.PanoramasGraph.FrequencyType.Target:
                            var _answer = await client.SendRangeMessage(startFreq, endFreq, RangeType.Target);
                            break;
                        default:
                            break;
                    }
                    break;
                case 2:
                    switch (frequencyType)
                    {
                        case PanoramasControl.PanoramasGraph.FrequencyType.Signal:
                            var answer = await client.SendRangeMessage(startFreq, endFreq, RangeType.Signal);
                            break;
                        case PanoramasControl.PanoramasGraph.FrequencyType.Target:
                            var _answer = await client.SendRangeMessage(startFreq, endFreq, RangeType.Target);
                            break;
                        default:
                            break;
                    }
                    break;
                case 3:
                    switch (frequencyType)
                    {
                        case PanoramasControl.PanoramasGraph.FrequencyType.Signal:
                            var answer = await client.SendRangeMessage(startFreq, endFreq, RangeType.Signal);
                            break;
                        case PanoramasControl.PanoramasGraph.FrequencyType.Target:
                            var _answer = await client.SendRangeMessage(startFreq, endFreq, RangeType.Target);
                            break;
                        default:
                            break;
                    }
                    break;
                case 4:
                    switch (frequencyType)
                    {
                        case PanoramasControl.PanoramasGraph.FrequencyType.Signal:
                            var answer = await client.SendRangeMessage(startFreq, endFreq, RangeType.Signal);
                            break;
                        case PanoramasControl.PanoramasGraph.FrequencyType.Target:
                            var _answer = await client.SendRangeMessage(startFreq, endFreq, RangeType.Target);
                            break;
                        default:
                            break;
                    }
                    break;
                case 5:
                    switch (frequencyType)
                    {
                        case PanoramasControl.PanoramasGraph.FrequencyType.Signal:
                            var answer = await client.SendRangeMessage(startFreq, endFreq, RangeType.Signal);
                            break;
                        case PanoramasControl.PanoramasGraph.FrequencyType.Target:
                            var _answer = await client.SendRangeMessage(startFreq, endFreq, RangeType.Target);
                            break;
                        default:
                            break;
                    }
                    break;
            }
            
        }

        private async void PLibrary_OnFreqArea(object sender, PanoramaLibrary.PLibrary.FrequencyType frequencyType, double startFreq, double endFreq)
        {
            if (frequencyType == PanoramaLibrary.PLibrary.FrequencyType.Signal)
            {
                var answer = await client.SendRangeMessage(startFreq, endFreq, RangeType.Signal);
            }
            if (frequencyType == PanoramaLibrary.PLibrary.FrequencyType.Target)
            {
                var answer = await client.SendRangeMessage(startFreq, endFreq, RangeType.Target);
            }
            if (frequencyType == PanoramaLibrary.PLibrary.FrequencyType.Recognition)
            {
                var answer = await client.SendRangeMessage(startFreq, endFreq, RangeType.Recognition);
            }
            if (frequencyType == PanoramaLibrary.PLibrary.FrequencyType.RecognitionM)
            {
                var answer = await client.SendRangeMessage(startFreq, endFreq, RangeType.RecognitionM);
            }
            if (frequencyType == PanoramaLibrary.PLibrary.FrequencyType.RecognitionL)
            {
                var answer = await client.SendRangeMessage(startFreq, endFreq, RangeType.RecognitionL);
            }
        }

        private void Client_GetCoordsMessageUpdate(CoordsMessage coordsMessage)
        {
            //Отправить в TechWindow
            SendRDMToTechWindow(coordsMessage);

            //Отправить на карту Ангелине
            ucMap.UpdateMapLinesTest(coordsMessage.Lat, coordsMessage.Lon);
            ucMap.UpdateMapPointsTest(coordsMessage.Lon, coordsMessage.Lat);
        }

        private void Client_GetExtraordinaryModeMessageUpdate(ModeMessage modeMessage)
        {
            switch (modeMessage.Mode)
            {
                case DapServerMode.PureView:
                    //подрежим обзор/поиск
                    wndViewModel.PropertyModel.ModeLabel = (byte)modeMessage.Mode;
                    wndViewModel.PropertyModel.ModeLabelCurrent = wndViewModel.PropertyModel.ModeLabel;
                    break;
                case DapServerMode.Control:
                    //подрежим контроль/местоопределения
                    wndViewModel.PropertyModel.ModeLabel = (byte)modeMessage.Mode;
                    wndViewModel.PropertyModel.ModeLabelCurrent = wndViewModel.PropertyModel.ModeLabel;
                    break;
                case DapServerMode.Recognition:
                    //подрежим операции распознования
                    wndViewModel.PropertyModel.ModeLed = (byte)modeMessage.Mode;
                    break;
            }
        }
    }
}
