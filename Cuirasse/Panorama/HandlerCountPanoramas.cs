﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Cuirasse
{
    public partial class MainWindow
    {
        CountPanoramas countPanoramas;
        GridLengthConverter GridLengthConverter = new GridLengthConverter();

        int countRightPanoramas = 0;
        int CountRightPanoramas
        {
            get => countRightPanoramas;
            set { countRightPanoramas = value; }
        }

        int countLeftPanoramas = 0;
        int CountLeftPanoramas
        {
            get => countLeftPanoramas;
            set { countLeftPanoramas = value; }
        }

        private CountPanoramas LoadComboboxData()
        {
            countPanoramas = YamlLoad<CountPanoramas>("PanoramaWindowSettings.yaml");
            if (countPanoramas == null)
            {
                countPanoramas = new CountPanoramas();
                YamlSave(countPanoramas, "PanoramaWindowSettings.yaml");
            }
            return countPanoramas;
        }

        private void InitComboxSettings()
        {
            countPanoramas = LoadComboboxData();

            PanoramaWindow.pLibrary.CountPanoramas = countPanoramas.ComboboxSettings.CountPanoramas;
            PanoramaWindow.pLibrary.GlobalRangeYmax = countPanoramas.PanoramaSettings.MaxAmplitudeYAxe;

            switch (countPanoramas.ComboboxSettings.CountCorrPanoramas)
            {
                case 0:
                    PanoramaWindow.pLibrary.CorrPanoramaIsVisibility(PanoramaLibrary.PLibrary.CorrelationVisibility.False);
                    break;
                case 1:
                    PanoramaWindow.pLibrary.CorrPanoramaIsVisibility(PanoramaLibrary.PLibrary.CorrelationVisibility.True);
                    break;
                default:
                    break;
            }

            PanoramaWindow.Panoramas.IndGraph = countPanoramas.SelectedPanorama.Index;
            PanoramaWindow.Panoramas.MaxAmplitudeYAxe = countPanoramas.PanoramaSettings.MaxAmplitudeYAxe;

            CountLeftPanoramas = countPanoramas.ComboboxSettings.CountPanoramas;
            CountRightPanoramas = countPanoramas.ComboboxSettings.CountCorrPanoramas;
        }

        private void PLibrary_OnSendCBIndex(int count)
        {
            CountLeftPanoramas = count;

            if (count != 0) 
            {
                PanoramaWindow.Panoramas.CountPanoramas = count;
                countPanoramas.ComboboxSettings.CountPanoramas = count;
                countPanoramas.SelectedPanorama.Index = PanoramaWindow.Panoramas.IndGraph;                

                PanoramaWindow.gridPanoramas.RowDefinitions[2].Height = (GridLength)GridLengthConverter.ConvertFromString("2*");

                if (countRightPanoramas != 0)
                {
                    PanoramaWindow.gridDown.ColumnDefinitions[0].Width = (GridLength)GridLengthConverter.ConvertFromString("2*");
                    PanoramaWindow.gridDown.ColumnDefinitions[2].Width = (GridLength)GridLengthConverter.ConvertFromString("*");
                }
                else
                {
                    PanoramaWindow.gridDown.ColumnDefinitions[0].Width = (GridLength)GridLengthConverter.ConvertFromString("*");
                    PanoramaWindow.gridDown.ColumnDefinitions[2].Width = (GridLength)GridLengthConverter.ConvertFromString("0");
                }
            }
            else 
            {
                countPanoramas.ComboboxSettings.CountPanoramas = count;

                if (PanoramaWindow.gridDown.ColumnDefinitions[2].Width == (GridLength)GridLengthConverter.ConvertFromString("0"))
                {
                    PanoramaWindow.gridPanoramas.RowDefinitions[2].Height = (GridLength)GridLengthConverter.ConvertFromString("0");
                }
                else
                {
                    PanoramaWindow.gridDown.ColumnDefinitions[0].Width = (GridLength)GridLengthConverter.ConvertFromString("0");
                    PanoramaWindow.gridDown.ColumnDefinitions[2].Width = (GridLength)GridLengthConverter.ConvertFromString("*");
                }
            }
            
            YamlSave(countPanoramas, "PanoramaWindowSettings.yaml");
        }

        private void PLibrary_OnSendCBCorrIndex(int count)
        {
            if (count != 0) 
            {
                PanoramaWindow.CorrFunctionControl.CountPanoramas = count;
                countPanoramas.ComboboxSettings.CountCorrPanoramas = count;
                countRightPanoramas = count;

                PanoramaWindow.gridPanoramas.RowDefinitions[2].Height = (GridLength)GridLengthConverter.ConvertFromString("2*");

                if (countLeftPanoramas != 0)
                {
                    PanoramaWindow.gridDown.ColumnDefinitions[0].Width = (GridLength)GridLengthConverter.ConvertFromString("2*");
                    PanoramaWindow.gridDown.ColumnDefinitions[2].Width = (GridLength)GridLengthConverter.ConvertFromString("*");
                }
                else
                {
                    PanoramaWindow.gridDown.ColumnDefinitions[0].Width = (GridLength)GridLengthConverter.ConvertFromString("0");
                    PanoramaWindow.gridDown.ColumnDefinitions[2].Width = (GridLength)GridLengthConverter.ConvertFromString("*");
                }
            }
            else 
            {
                if (PanoramaWindow.gridDown.ColumnDefinitions[0].Width == (GridLength)GridLengthConverter.ConvertFromString("0"))
                {
                    PanoramaWindow.gridPanoramas.RowDefinitions[2].Height = (GridLength)GridLengthConverter.ConvertFromString("0");
                }
                else
                {
                    PanoramaWindow.gridDown.ColumnDefinitions[0].Width = (GridLength)GridLengthConverter.ConvertFromString("*");
                    PanoramaWindow.gridDown.ColumnDefinitions[2].Width = (GridLength)GridLengthConverter.ConvertFromString("0");
                }
            }

            YamlSave(countPanoramas, "CountPanoramas.yaml");
        }

        private void PLibrary_OnCorrelationVisibility(PanoramaLibrary.PLibrary.CorrelationVisibility correlationVisibility)
        {
            switch (correlationVisibility)
            {
                case PanoramaLibrary.PLibrary.CorrelationVisibility.True:
                    countPanoramas.ComboboxSettings.CountCorrPanoramas = 1;
                    CountRightPanoramas = 1;

                    PanoramaWindow.gridPanoramas.RowDefinitions[2].Height = (GridLength)GridLengthConverter.ConvertFromString("2*");

                    if (countLeftPanoramas != 0)
                    {
                        PanoramaWindow.gridDown.ColumnDefinitions[0].Width = (GridLength)GridLengthConverter.ConvertFromString("2*");
                        PanoramaWindow.gridDown.ColumnDefinitions[2].Width = (GridLength)GridLengthConverter.ConvertFromString("*");
                    }
                    else
                    {
                        PanoramaWindow.gridDown.ColumnDefinitions[0].Width = (GridLength)GridLengthConverter.ConvertFromString("0");
                        PanoramaWindow.gridDown.ColumnDefinitions[2].Width = (GridLength)GridLengthConverter.ConvertFromString("*");
                    }
                    break;
                case PanoramaLibrary.PLibrary.CorrelationVisibility.False:
                    countPanoramas.ComboboxSettings.CountCorrPanoramas = 0;
                    CountRightPanoramas = 0;

                    if (PanoramaWindow.gridDown.ColumnDefinitions[0].Width == (GridLength)GridLengthConverter.ConvertFromString("0"))
                    {
                        PanoramaWindow.gridPanoramas.RowDefinitions[2].Height = (GridLength)GridLengthConverter.ConvertFromString("0");
                    }
                    else
                    {
                        PanoramaWindow.gridDown.ColumnDefinitions[0].Width = (GridLength)GridLengthConverter.ConvertFromString("*");
                        PanoramaWindow.gridDown.ColumnDefinitions[2].Width = (GridLength)GridLengthConverter.ConvertFromString("0");
                    }
                    break;
                default:
                    break;
            }
            YamlSave(countPanoramas, "PanoramaWindowSettings.yaml");
        }
    }
}
