﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cuirasse
{
    public partial class MainWindow
    {
        private void SetLanguagePanoramas(DllCuirassemProperties.Models.Languages language) 
        {
            try
            {
                switch (language)
                {
                    case DllCuirassemProperties.Models.Languages.RU:
                        PanoramaWindow.Panoramas.Language("RU");
                        break;
                    case DllCuirassemProperties.Models.Languages.EN:
                        PanoramaWindow.Panoramas.Language("EN"); 
                        break;
                    default:
                        PanoramaWindow.Panoramas.Language("EN");
                        break;
                }
            }
            catch { }
        }
    }
}
