﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cuirasse
{
    public partial class MainWindow
    {
        bool isConnect = false;

        private void Client_IsConnected(bool isConnected)
        {
            DispatchIfNecessary(async () =>
            {
                isConnect = isConnected;
                if (isConnected)
                {
                    ServerControlConnection.ShowConnect();
                    var answer = await client.SetFilters((short)PanoramaWindow.pLibrary.Threshold, DAPprotocols.ThresholdType.RIThreshold);
                    var answerCorrThreshold = await client.GetFilters(DAPprotocols.ThresholdType.CorrThreshold);
                    //Send to Serega CorrPanoram
                    PanoramaWindow.CorrFunctionControl.UpdateCorrelationTheresold(answerCorrThreshold.Threshold / 100f);
                }
                else
                {
                    ServerControlConnection.ShowDisconnect();
                }
            });
        }

        private void Client_IsRead(bool isRead)
        {
            DispatchIfNecessary(() => 
            {
                ServerControlConnection.ShowRead();
            });            
        }

        private void Client_IsWrite(bool isWrite)
        {
            DispatchIfNecessary(() =>
            {
                ServerControlConnection.ShowWrite();
            });
        }

        private void Client_AnswerSetMode(DAPprotocols.ModeMessage modeMessage)
        {
            if (modeMessage?.Header.ErrorCode == 0)
            {
                if (modeMessage.Mode == DAPprotocols.DapServerMode.Stop)
                {
                    PanoramaWindow.Panoramas.UpdateMode(0);
                }
                if (modeMessage.Mode == DAPprotocols.DapServerMode.RadioIntelligence)
                {
                    PanoramaWindow.Panoramas.UpdateMode(1);
                }
            }
        }


        private void AnswerGetSpectrum(DAPprotocols.GetSpectrumResponse spectrumResponse)
        {
            if (spectrumResponse?.Header.ErrorCode == 0)
            {
                if (spectrumResponse.Spectrum != null)
                {
                    switch (spectrumResponse.PostNumber)
                    {
                        case 0:
                            if (spectrumResponse.Spectrum != null)
                            {
                                //Draw Spectrum Main
                                switch (spectrumResponse.Picker)
                                {
                                    case DAPprotocols.ChannelPicker.Channel1: PanoramaWindow.pLibrary.IQSpectrumPainted(spectrumResponse.MinFrequencyMHz, spectrumResponse.MaxFrequencyMHz, spectrumResponse.Spectrum, 1); break;
                                    case DAPprotocols.ChannelPicker.Channel2: PanoramaWindow.pLibrary.IQSpectrumPainted(spectrumResponse.MinFrequencyMHz, spectrumResponse.MaxFrequencyMHz, spectrumResponse.Spectrum, 2); break;
                                    case DAPprotocols.ChannelPicker.Channel3: PanoramaWindow.pLibrary.IQSpectrumPainted(spectrumResponse.MinFrequencyMHz, spectrumResponse.MaxFrequencyMHz, spectrumResponse.Spectrum, 3); break;
                                    case DAPprotocols.ChannelPicker.Channel4: PanoramaWindow.pLibrary.IQSpectrumPainted(spectrumResponse.MinFrequencyMHz, spectrumResponse.MaxFrequencyMHz, spectrumResponse.Spectrum, 4); break;
                                    case DAPprotocols.ChannelPicker.ChannelMax: PanoramaWindow.pLibrary.IQSpectrumPainted(spectrumResponse.MinFrequencyMHz, spectrumResponse.MaxFrequencyMHz, spectrumResponse.Spectrum, 5); break;
                                    case DAPprotocols.ChannelPicker.ChannelMedian: PanoramaWindow.pLibrary.IQSpectrumPainted(spectrumResponse.MinFrequencyMHz, spectrumResponse.MaxFrequencyMHz, spectrumResponse.Spectrum, 6); break;
                                    case DAPprotocols.ChannelPicker.ChannelSum: PanoramaWindow.pLibrary.IQSpectrumPainted(spectrumResponse.MinFrequencyMHz, spectrumResponse.MaxFrequencyMHz, spectrumResponse.Spectrum, 7); break;
                                }                                
                            }
                            break;
                        case 1:
                            if (spectrumResponse.Spectrum != null)
                            {
                                //Draw Spectrum First                            
                                switch (spectrumResponse.Picker)
                                {
                                    case DAPprotocols.ChannelPicker.Channel1: PanoramaWindow.Panoramas.Converter(spectrumResponse.Spectrum, 1, 1, spectrumResponse.MinFrequencyMHz, spectrumResponse.MaxFrequencyMHz); break;
                                    case DAPprotocols.ChannelPicker.Channel2: PanoramaWindow.Panoramas.Converter(spectrumResponse.Spectrum, 1, 2, spectrumResponse.MinFrequencyMHz, spectrumResponse.MaxFrequencyMHz); break;
                                    case DAPprotocols.ChannelPicker.Channel3: PanoramaWindow.Panoramas.Converter(spectrumResponse.Spectrum, 1, 3, spectrumResponse.MinFrequencyMHz, spectrumResponse.MaxFrequencyMHz); break;
                                    case DAPprotocols.ChannelPicker.Channel4: PanoramaWindow.Panoramas.Converter(spectrumResponse.Spectrum, 1, 4, spectrumResponse.MinFrequencyMHz, spectrumResponse.MaxFrequencyMHz); break;
                                    case DAPprotocols.ChannelPicker.ChannelMax: PanoramaWindow.Panoramas.Converter(spectrumResponse.Spectrum, 1, 5, spectrumResponse.MinFrequencyMHz, spectrumResponse.MaxFrequencyMHz); break;
                                    case DAPprotocols.ChannelPicker.ChannelMedian: PanoramaWindow.Panoramas.Converter(spectrumResponse.Spectrum, 1, 6, spectrumResponse.MinFrequencyMHz, spectrumResponse.MaxFrequencyMHz); break;
                                    case DAPprotocols.ChannelPicker.ChannelSum: PanoramaWindow.Panoramas.Converter(spectrumResponse.Spectrum, 1, 7, spectrumResponse.MinFrequencyMHz, spectrumResponse.MaxFrequencyMHz); break;
                                }
                            }
                            break;
                        case 2:
                            if (spectrumResponse.Spectrum != null)
                            {
                                //Draw Spectrum Second 
                                switch (spectrumResponse.Picker)
                                {
                                    case DAPprotocols.ChannelPicker.Channel1: PanoramaWindow.Panoramas.Converter(spectrumResponse.Spectrum, 2, 1, spectrumResponse.MinFrequencyMHz, spectrumResponse.MaxFrequencyMHz); break;
                                    case DAPprotocols.ChannelPicker.Channel2: PanoramaWindow.Panoramas.Converter(spectrumResponse.Spectrum, 2, 2, spectrumResponse.MinFrequencyMHz, spectrumResponse.MaxFrequencyMHz); break;
                                    case DAPprotocols.ChannelPicker.Channel3: PanoramaWindow.Panoramas.Converter(spectrumResponse.Spectrum, 2, 3, spectrumResponse.MinFrequencyMHz, spectrumResponse.MaxFrequencyMHz); break;
                                    case DAPprotocols.ChannelPicker.Channel4: PanoramaWindow.Panoramas.Converter(spectrumResponse.Spectrum, 2, 4, spectrumResponse.MinFrequencyMHz, spectrumResponse.MaxFrequencyMHz); break;
                                    case DAPprotocols.ChannelPicker.ChannelMax: PanoramaWindow.Panoramas.Converter(spectrumResponse.Spectrum, 2, 5, spectrumResponse.MinFrequencyMHz, spectrumResponse.MaxFrequencyMHz); break;
                                    case DAPprotocols.ChannelPicker.ChannelMedian: PanoramaWindow.Panoramas.Converter(spectrumResponse.Spectrum, 2, 6, spectrumResponse.MinFrequencyMHz, spectrumResponse.MaxFrequencyMHz); break;
                                    case DAPprotocols.ChannelPicker.ChannelSum: PanoramaWindow.Panoramas.Converter(spectrumResponse.Spectrum, 2, 7, spectrumResponse.MinFrequencyMHz, spectrumResponse.MaxFrequencyMHz); break;
                                }
                            }
                            break;
                        case 3:
                            if (spectrumResponse.Spectrum != null)
                            {
                                //Draw Spectrum Third
                                switch (spectrumResponse.Picker)
                                {
                                    case DAPprotocols.ChannelPicker.Channel1: PanoramaWindow.Panoramas.Converter(spectrumResponse.Spectrum, 3, 1, spectrumResponse.MinFrequencyMHz, spectrumResponse.MaxFrequencyMHz); break;
                                    case DAPprotocols.ChannelPicker.Channel2: PanoramaWindow.Panoramas.Converter(spectrumResponse.Spectrum, 3, 2, spectrumResponse.MinFrequencyMHz, spectrumResponse.MaxFrequencyMHz); break;
                                    case DAPprotocols.ChannelPicker.Channel3: PanoramaWindow.Panoramas.Converter(spectrumResponse.Spectrum, 3, 3, spectrumResponse.MinFrequencyMHz, spectrumResponse.MaxFrequencyMHz); break;
                                    case DAPprotocols.ChannelPicker.Channel4: PanoramaWindow.Panoramas.Converter(spectrumResponse.Spectrum, 3, 4, spectrumResponse.MinFrequencyMHz, spectrumResponse.MaxFrequencyMHz); break;
                                    case DAPprotocols.ChannelPicker.ChannelMax: PanoramaWindow.Panoramas.Converter(spectrumResponse.Spectrum, 3, 5, spectrumResponse.MinFrequencyMHz, spectrumResponse.MaxFrequencyMHz); break;
                                    case DAPprotocols.ChannelPicker.ChannelMedian: PanoramaWindow.Panoramas.Converter(spectrumResponse.Spectrum, 3, 6, spectrumResponse.MinFrequencyMHz, spectrumResponse.MaxFrequencyMHz); break;
                                    case DAPprotocols.ChannelPicker.ChannelSum: PanoramaWindow.Panoramas.Converter(spectrumResponse.Spectrum, 3, 7, spectrumResponse.MinFrequencyMHz, spectrumResponse.MaxFrequencyMHz); break;
                                }
                            }
                            break;
                        case 4:
                            if (spectrumResponse.Spectrum != null)
                            {
                                //Draw Spectrum Four 
                                switch (spectrumResponse.Picker)
                                {
                                    case DAPprotocols.ChannelPicker.Channel1: PanoramaWindow.Panoramas.Converter(spectrumResponse.Spectrum, 4, 1, spectrumResponse.MinFrequencyMHz, spectrumResponse.MaxFrequencyMHz); break;
                                    case DAPprotocols.ChannelPicker.Channel2: PanoramaWindow.Panoramas.Converter(spectrumResponse.Spectrum, 4, 2, spectrumResponse.MinFrequencyMHz, spectrumResponse.MaxFrequencyMHz); break;
                                    case DAPprotocols.ChannelPicker.Channel3: PanoramaWindow.Panoramas.Converter(spectrumResponse.Spectrum, 4, 3, spectrumResponse.MinFrequencyMHz, spectrumResponse.MaxFrequencyMHz); break;
                                    case DAPprotocols.ChannelPicker.Channel4: PanoramaWindow.Panoramas.Converter(spectrumResponse.Spectrum, 4, 4, spectrumResponse.MinFrequencyMHz, spectrumResponse.MaxFrequencyMHz); break;
                                    case DAPprotocols.ChannelPicker.ChannelMax: PanoramaWindow.Panoramas.Converter(spectrumResponse.Spectrum, 4, 5, spectrumResponse.MinFrequencyMHz, spectrumResponse.MaxFrequencyMHz); break;
                                    case DAPprotocols.ChannelPicker.ChannelMedian: PanoramaWindow.Panoramas.Converter(spectrumResponse.Spectrum, 4, 6, spectrumResponse.MinFrequencyMHz, spectrumResponse.MaxFrequencyMHz); break;
                                    case DAPprotocols.ChannelPicker.ChannelSum: PanoramaWindow.Panoramas.Converter(spectrumResponse.Spectrum, 4, 7, spectrumResponse.MinFrequencyMHz, spectrumResponse.MaxFrequencyMHz); break;
                                }
                            }
                            break;
                        case 5:
                            if (spectrumResponse.Spectrum != null)
                            {
                                //Five
                                switch (spectrumResponse.Picker)
                                {
                                    case DAPprotocols.ChannelPicker.Channel1: PanoramaWindow.Panoramas.Converter(spectrumResponse.Spectrum, 5, 1, spectrumResponse.MinFrequencyMHz, spectrumResponse.MaxFrequencyMHz); break;
                                    case DAPprotocols.ChannelPicker.Channel2: PanoramaWindow.Panoramas.Converter(spectrumResponse.Spectrum, 5, 2, spectrumResponse.MinFrequencyMHz, spectrumResponse.MaxFrequencyMHz); break;
                                    case DAPprotocols.ChannelPicker.Channel3: PanoramaWindow.Panoramas.Converter(spectrumResponse.Spectrum, 5, 3, spectrumResponse.MinFrequencyMHz, spectrumResponse.MaxFrequencyMHz); break;
                                    case DAPprotocols.ChannelPicker.Channel4: PanoramaWindow.Panoramas.Converter(spectrumResponse.Spectrum, 5, 4, spectrumResponse.MinFrequencyMHz, spectrumResponse.MaxFrequencyMHz); break;
                                    case DAPprotocols.ChannelPicker.ChannelMax: PanoramaWindow.Panoramas.Converter(spectrumResponse.Spectrum, 5, 5, spectrumResponse.MinFrequencyMHz, spectrumResponse.MaxFrequencyMHz); break;
                                    case DAPprotocols.ChannelPicker.ChannelMedian: PanoramaWindow.Panoramas.Converter(spectrumResponse.Spectrum, 5, 6, spectrumResponse.MinFrequencyMHz, spectrumResponse.MaxFrequencyMHz); break;
                                    case DAPprotocols.ChannelPicker.ChannelSum: PanoramaWindow.Panoramas.Converter(spectrumResponse.Spectrum, 5, 7, spectrumResponse.MinFrequencyMHz, spectrumResponse.MaxFrequencyMHz); break;
                                }
                            }
                            break;
                    }
                }
            }
        }

        private void Client_GetCorrFuncUpdate(DAPprotocols.GetCorrFuncResponse corrFuncResponse)
        {
            if (corrFuncResponse?.Header.ErrorCode == 0)
            {
                if (corrFuncResponse.CorrFunc0 != null || corrFuncResponse.CorrFunc1 != null || corrFuncResponse.CorrFunc2 != null)
                {
                    PanoramaWindow.CorrFunctionControl.Converter(corrFuncResponse.CorrFunc0, 1, corrFuncResponse.FuncNumber, corrFuncResponse.FrequencykHz, corrFuncResponse.BandwidthkHz);
                    PanoramaWindow.CorrFunctionControl.Converter(corrFuncResponse.CorrFunc1, 2, corrFuncResponse.FuncNumber, corrFuncResponse.FrequencykHz, corrFuncResponse.BandwidthkHz);
                    PanoramaWindow.CorrFunctionControl.Converter(corrFuncResponse.CorrFunc2, 3, corrFuncResponse.FuncNumber, corrFuncResponse.FrequencykHz, corrFuncResponse.BandwidthkHz);
                    PanoramaWindow.CorrFunctionControl.Converter(corrFuncResponse.CorrFunc3, 0, corrFuncResponse.FuncNumber, corrFuncResponse.FrequencykHz, corrFuncResponse.BandwidthkHz);
                }
            }
        }

        private void Client_GetTausMessageUpdate(DAPprotocols.TausMessage tausMessage)
        {
            //Отправить задержки к Серёге
            if (tausMessage != null)
                PanoramaWindow.CorrFunctionControl.UpdateCorrelationTaus(tausMessage.tau12, tausMessage.tau13, tausMessage.tau14);
        }

    }
}
