﻿using DAPServerClient2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace Cuirasse
{
    public partial class MainWindow
    {
        Client client = new Client();
        PanoramaWindow PanoramaWindow = new PanoramaWindow();

        public void DispatchIfNecessary(Action action)
        {
            if (!Dispatcher.CheckAccess())
                Dispatcher.Invoke(action);
            else
                action.Invoke();
        }

        private async void ServerConnection_Click(object sender, RoutedEventArgs e)
        {
            DispatchIfNecessary(async () =>
            {
                if (!isConnect)
                {
                    CancellationTokenSource cts = new CancellationTokenSource();
                    CancellationToken token = cts.Token;
                    int timeout = 3000;
                    var task = client.ConnectToServer2("127.0.0.1", token);
                    var task2 = W3s(timeout);

                    if (await Task.WhenAny(task, task2) == task)
                    {
                        Console.WriteLine("task completed within timeout");
                        ServerControlConnection.ShowConnect();
                        ServerControlConnection.ShowRead();
                        ServerControlConnection.ShowWrite();
                    }
                    else
                    {
                        cts.Cancel();
                        ServerControlConnection.ShowDisconnect();
                        ServerControlConnection.ShowRead();
                        ServerControlConnection.ShowWrite();
                        Console.WriteLine("timeout logic");
                        MessageBox.Show("Invalid IP address or Server not found", "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
                    }

                    Console.WriteLine("continue work");

                    async Task W3s(int t)
                    {
                        await Task.Delay(t);
                    }
                }
                else
                {
                    client.DisconnectFromServer();
                    PanoramaWindow.pLibrary.Mode = 0;
                    ServerControlConnection.ShowDisconnect();
                }
            });
        }
    }
}
