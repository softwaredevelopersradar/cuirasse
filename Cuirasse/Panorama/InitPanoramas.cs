﻿using DAPprotocols;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cuirasse
{
    public partial class MainWindow
    {
        private void InitPanoramsGraph()
        {
            PanoramaWindow.Panoramas.NeedSpectrumRequest += PanoramasGraphs_NeedSpectrumRequest;
            PanoramaWindow.Panoramas.NeedSpectrumRequest2 += PanoramasGraphs_NeedSpectrumRequest2;
            PanoramaWindow.Panoramas.NeedSpectrumRequest3 += PanoramasGraphs_NeedSpectrumRequest3;
            PanoramaWindow.Panoramas.NeedSpectrumRequest4 += PanoramasGraphs_NeedSpectrumRequest4;
            PanoramaWindow.Panoramas.NeedSpectrumRequest5 += PanoramasGraphs_NeedSpectrumRequest5;
            PanoramaWindow.Panoramas.OnCheckGraph += PanoramasGraphs_OnCheckGraph;
            PanoramaWindow.Panoramas.OnFreqArea += Panoramas_OnFreqArea;

            PanoramaWindow.CorrFunctionControl.OnThresholdChanged += CorrFunctionControl_OnThresholdChanged;
        }

        private async void CorrFunctionControl_OnThresholdChanged(double value)
        {
            // var answer = await client.SetFilters((short)(value * 100), ThresholdType.CorrThreshold);

            basicProperties.Global.Correlation.PathThroughCorrelationThreshold = (float)value;
            GlobalPropertyToDB(basicProperties.Global);
        }

        private void InitTextBoxManager()
        {   
            var textBoxSettings = LoadTextBoxData();
            PanoramaWindow.Panoramas.Frequency_1 = textBoxSettings.textBoxSettings.Frequency_1;
            PanoramaWindow.Panoramas.RangeFrq_1 = textBoxSettings.textBoxSettings.Range_1;
            PanoramaWindow.Panoramas.Frequency_2 = textBoxSettings.textBoxSettings.Frequency_2;
            PanoramaWindow.Panoramas.RangeFrq_2 = textBoxSettings.textBoxSettings.Range_2;
            PanoramaWindow.Panoramas.Frequency_3 = textBoxSettings.textBoxSettings.Frequency_3;
            PanoramaWindow.Panoramas.RangeFrq_3 = textBoxSettings.textBoxSettings.Range_3;
            PanoramaWindow.Panoramas.Frequency_4 = textBoxSettings.textBoxSettings.Frequency_4;
            PanoramaWindow.Panoramas.RangeFrq_4 = textBoxSettings.textBoxSettings.Range_4;
            PanoramaWindow.Panoramas.Frequency_5 = textBoxSettings.textBoxSettings.Frequency_5;
            PanoramaWindow.Panoramas.RangeFrq_5 = textBoxSettings.textBoxSettings.Range_5;
        }

        private void PLibrary_OnSetFreqToPanoram(object sender, double begin, double end, double frequency)
        {
            double range = Math.Abs(end - begin);
            
            switch (PanoramaWindow.Panoramas.IndGraph)
            {
                case 1:
                    PanoramaWindow.Panoramas.Frequency_1 = frequency;
                    PanoramaWindow.Panoramas.RangeFrq_1 = range;
                    break;
                case 2:
                    PanoramaWindow.Panoramas.Frequency_2 = frequency;
                    PanoramaWindow.Panoramas.RangeFrq_2 = range;
                    break;
                case 3:
                    PanoramaWindow.Panoramas.Frequency_3 = frequency;
                    PanoramaWindow.Panoramas.RangeFrq_3 = range;
                    break;
                case 4:
                    PanoramaWindow.Panoramas.Frequency_4 = frequency;
                    PanoramaWindow.Panoramas.RangeFrq_4 = range;
                    break;
                case 5:
                    PanoramaWindow.Panoramas.Frequency_5 = frequency;
                    PanoramaWindow.Panoramas.RangeFrq_5 = range;
                    break;
                default:
                    PanoramaWindow.Panoramas.Frequency_1 = frequency;
                    PanoramaWindow.Panoramas.RangeFrq_1 = range;
                    break;
            }

            var textBoxSettings = new TextBoxData();
            textBoxSettings.textBoxSettings.Frequency_1 = PanoramaWindow.Panoramas.Frequency_1;
            textBoxSettings.textBoxSettings.Range_1 = PanoramaWindow.Panoramas.RangeFrq_1;
            textBoxSettings.textBoxSettings.Frequency_2 = PanoramaWindow.Panoramas.Frequency_2;
            textBoxSettings.textBoxSettings.Range_2 = PanoramaWindow.Panoramas.RangeFrq_2;
            textBoxSettings.textBoxSettings.Frequency_3 = PanoramaWindow.Panoramas.Frequency_3;
            textBoxSettings.textBoxSettings.Range_3 = PanoramaWindow.Panoramas.RangeFrq_3;
            textBoxSettings.textBoxSettings.Frequency_4 = PanoramaWindow.Panoramas.Frequency_4;
            textBoxSettings.textBoxSettings.Range_4 = PanoramaWindow.Panoramas.RangeFrq_4;
            textBoxSettings.textBoxSettings.Frequency_5 = PanoramaWindow.Panoramas.Frequency_5;
            textBoxSettings.textBoxSettings.Range_5 = PanoramaWindow.Panoramas.RangeFrq_5;
            YamlSave(textBoxSettings, "TextBoxData.yaml");
        }

        private void PanoramasGraphs_OnCheckGraph(object sender, byte index)
        {
            countPanoramas.SelectedPanorama.Index = PanoramaWindow.Panoramas.IndGraph;
            countPanoramas.ComboboxSettings.CountPanoramas = PanoramaWindow.Panoramas.CountPanoramas;
            YamlSave(countPanoramas, "PanoramaWindowSettings.yaml");
        }

        private void PanoramasGraphs_Access(DllCuirassemProperties.Models.AccessTypes accessTypes) 
        {
            switch (accessTypes) 
            {
                case DllCuirassemProperties.Models.AccessTypes.Operator:
                    PanoramaWindow.CorrFunctionControl.DebugVisible = 0;
                    break;
                case DllCuirassemProperties.Models.AccessTypes.Commandir:
                    PanoramaWindow.CorrFunctionControl.DebugVisible = 0;
                    break;
                case DllCuirassemProperties.Models.AccessTypes.Admin:
                    PanoramaWindow.CorrFunctionControl.DebugVisible = 1;
                    break;
            }            
        }

        private void InitCorrGraphProperties() 
        {
            PanoramaWindow.CorrFunctionControl.XAxesCorrGraphSetMin = -3200;
            PanoramaWindow.CorrFunctionControl.XAxesCorrGraphSetMax = 3200;
            PanoramaWindow.CorrFunctionControl.CountPointsSet = 801;
        }

        private void InitDefaultChannels()
        {
            PanoramaWindow.pLibrary.DefaultChannels(basicProperties.Local.Spectrum.Chanel_1,
                basicProperties.Local.Spectrum.Chanel_2,
                basicProperties.Local.Spectrum.Chanel_3,
                basicProperties.Local.Spectrum.Chanel_4);
        }
    }
}
