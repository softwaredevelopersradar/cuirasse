﻿using InheritorsEventArgs;
using KirasaModelsDBLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Cuirasse
{
    public partial class MainWindow
    {
        public void PanoramaImportRIFreqs(List<TableFreqRanges> listSRanges)
        {
            List<double> lMinFreqs = new List<double>();
            List<double> lMaxFreqs = new List<double>();

            for (int i = 0; i < listSRanges.Count; i++)
            {
                if (listSRanges[i].IsActive)
                {
                    lMinFreqs.Add(listSRanges[i].FreqMinKHz / 1000d);
                    lMaxFreqs.Add(listSRanges[i].FreqMaxKHz / 1000d);
                }
            }

            double[] MinFreqs = lMinFreqs.ToArray();
            double[] MaxFreqs = lMaxFreqs.ToArray();

            DispatchIfNecessary(() =>
            {
                PanoramaWindow.pLibrary.ImportFreqs(PanoramaLibrary.PLibrary.FreqsType.FreqsRI, MinFreqs, MaxFreqs);
                PanoramaWindow.Panoramas.CheckFrequency(MinFreqs, MaxFreqs);
            });
        }
    }
}
