﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Cuirasse
{
    public partial class MainWindow
    {

        public bool IsPanoramaHide = false;
        public void InitPanorama()
        {            
            PanoramaWindow.Hide();

            PanoramaWindow.OnClickBClose += new EventHandler(PanoramaWnd_OnClickBClose);
        }

        private void PanoramaWnd_OnClickBClose(object sender, System.EventArgs e)
        {
            bPanorama.IsChecked = false;
            IsPanoramaHide = false;
        }

        private void bPanorama_Click(object sender, RoutedEventArgs e)
        {
            if (bPanorama.IsChecked.Value)
            {
                PanoramaWindow.Show();
                IsPanoramaHide = true;
            }
            else
            {
                PanoramaWindow.Hide();
                IsPanoramaHide = false;
            }
        }

    }
}
