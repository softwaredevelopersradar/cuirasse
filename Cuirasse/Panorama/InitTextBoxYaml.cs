﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cuirasse
{
    public partial class MainWindow
    {
        private TextBoxData LoadTextBoxData()
        {
            var textBoxSettings = YamlLoad<TextBoxData>("TextBoxData.yaml");
            if (textBoxSettings == null)
            {
                textBoxSettings = new TextBoxData();
                YamlSave(textBoxSettings, "TextBoxData.yaml");
            }
            return textBoxSettings;
        }

        public class TextBoxData
        {
            public TextBoxData()
            {
                textBoxSettings = new TextBoxSettings();
            }
            public TextBoxSettings textBoxSettings { get; set; }
        }

        public class TextBoxSettings
        {
            public TextBoxSettings()
            {
                Frequency_1 = 180;
                Range_1 = 62.5;
                Frequency_2 = 250;
                Range_2 = 62.5;
                Frequency_3 = 500;
                Range_3 = 62.5;
                Frequency_4 = 1000;
                Range_4 = 62.5;
                Frequency_5 = 2400;
                Range_5 = 62.5;
            }

            public double Frequency_1 { get; set; }
            public double Range_1 { get; set; }
            public double Frequency_2 { get; set; }
            public double Range_2 { get; set; }
            public double Frequency_3 { get; set; }
            public double Range_3 { get; set; }
            public double Frequency_4 { get; set; }
            public double Range_4 { get; set; }
            public double Frequency_5 { get; set; }
            public double Range_5 { get; set; }
        }
    }
}
