﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cuirasse
{
    public class MyMapper
    {
        public MyMapper()
        { }

        public DllCuirassemProperties.Models.GlobalProperties DBToCtrl(KirasaModelsDBLib.GlobalProperties e)
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<KirasaModelsDBLib.GlobalProperties, DllCuirassemProperties.Models.GlobalProperties>()

                   .ForPath(dest => dest.Common.CenterZoneLatitude, map => map.MapFrom(src => src.Common.CenterLatitude))
                   .ForPath(dest => dest.Common.CenterZoneLongitude, map => map.MapFrom(src => src.Common.CenterLongitude))
                   .ForPath(dest => dest.Common.ZoneAlarm, map => map.MapFrom(src => src.Common.ZoneAlarm))
                   .ForPath(dest => dest.Common.ZoneAttention, map => map.MapFrom(src => src.Common.ZoneAttention))
                   .ForPath(dest => dest.Common.OperatorLocationLatitude, map => map.MapFrom(src => src.Common.OperatorLatitude))
                   .ForPath(dest => dest.Common.OperatorLocationLongitude, map => map.MapFrom(src => src.Common.OperatorLongitude))

                   .ForPath(dest => dest.RadioIntelegence.FrequencyAccuracy, opts => opts.MapFrom(src => src.RadioIntelegence.FrequencyAccuracy))
                   .ForPath(dest => dest.RadioIntelegence.BandAccuracy, opts => opts.MapFrom(src => src.RadioIntelegence.BandAccuracy))
                   .ForPath(dest => dest.RadioIntelegence.SamplingFrequency, opts => opts.MapFrom(src => src.RadioIntelegence.SamplingFrequency))


                   .ForPath(dest => dest.KalmanFilterForDelay.StaticArrayError, opts => opts.MapFrom(src => src.KalmanFilterForDelay.StaticErrorMatrix))
                   .ForPath(dest => dest.KalmanFilterForDelay.DelayMeasurementError, opts => opts.MapFrom(src => src.KalmanFilterForDelay.DelayMeasurementError))
                   .ForPath(dest => dest.KalmanFilterForDelay.NoiseAccelerationError, opts => opts.MapFrom(src => src.KalmanFilterForDelay.AccelerationNoise))
                   .ForPath(dest => dest.KalmanFilterForDelay.TimeHoldToDetectSource, opts => opts.MapFrom(src => src.KalmanFilterForDelay.TimeHoldToDetect))
                   .ForPath(dest => dest.KalmanFilterForDelay.MaxResetTime, opts => opts.MapFrom(src => src.KalmanFilterForDelay.MaxResetTime))
                   .ForPath(dest => dest.KalmanFilterForDelay.MinPointObject, opts => opts.MapFrom(src => src.KalmanFilterForDelay.MinAirObjPoints))


                   .ForPath(dest => dest.KalmanFilterForCoordinate.StaticArrayError, opts => opts.MapFrom(src => src.KalmanFilterForCoordinate.StaticErrorMatrix))
                   .ForPath(dest => dest.KalmanFilterForCoordinate.MaxUAVSpeed, opts => opts.MapFrom(src => src.KalmanFilterForCoordinate.SpeedUavMax))
                   .ForPath(dest => dest.KalmanFilterForCoordinate.TimeHoldToDetectSource, opts => opts.MapFrom(src => src.KalmanFilterForCoordinate.TimeHoldToDetect))
                   .ForPath(dest => dest.KalmanFilterForCoordinate.RmsX, opts => opts.MapFrom(src => src.KalmanFilterForCoordinate.RmsX))
                   .ForPath(dest => dest.KalmanFilterForCoordinate.RmsY, opts => opts.MapFrom(src => src.KalmanFilterForCoordinate.RmsY))
                   .ForPath(dest => dest.KalmanFilterForCoordinate.RmsZ, opts => opts.MapFrom(src => src.KalmanFilterForCoordinate.RmsZ))
                   .ForPath(dest => dest.KalmanFilterForCoordinate.MaxResetTime, opts => opts.MapFrom(src => src.KalmanFilterForCoordinate.MaxResetTime))
                   .ForPath(dest => dest.KalmanFilterForCoordinate.CoordinateDefinitionError, opts => opts.MapFrom(src => src.KalmanFilterForCoordinate.CoordinateDefinitionError))
                   .ForPath(dest => dest.KalmanFilterForCoordinate.StrobeDistance, opts => opts.MapFrom(src => src.KalmanFilterForCoordinate.StrobeDistance))
                   .ForPath(dest => dest.KalmanFilterForCoordinate.TauError, opts => opts.MapFrom(src => src.KalmanFilterForCoordinate.TauError))
                   .ForPath(dest => dest.KalmanFilterForCoordinate.XYZError, opts => opts.MapFrom(src => src.KalmanFilterForCoordinate.XyzError))
                   .ForPath(dest => dest.KalmanFilterForCoordinate.MinPointAirObject, opts => opts.MapFrom(src => src.KalmanFilterForCoordinate.MinAirObjPoints))


                   .ForPath(dest => dest.Correlation.AmountCorrelationQuery, opts => opts.MapFrom(src => src.Correlation.AmountCorrelationQuery))
                   .ForPath(dest => dest.Correlation.PathThroughCorrelationThreshold, opts => opts.MapFrom(src => src.Correlation.PathThroughCorrelationThreshold))
                   .ForPath(dest => dest.Correlation.CoefficientBorderDеlayEstimation, opts => opts.MapFrom(src => src.Correlation.CoefficientBorderDеlayEstimation))

                   .ForPath(dest => dest.DRMFilters.IsImSolution, opts => opts.MapFrom(src => src.DRMFilters.IsImaginarySolution))
                   .ForPath(dest => dest.DRMFilters.StartElevation, opts => opts.MapFrom(src => src.DRMFilters.StartElevation))
                   .ForPath(dest => dest.DRMFilters.Alpha, opts => opts.MapFrom(src => src.DRMFilters.Alpha))
                   ;

            });

            IMapper iMapper = config.CreateMapper();

            var destination = iMapper.Map<KirasaModelsDBLib.GlobalProperties, DllCuirassemProperties.Models.GlobalProperties>(e);
            return destination;
        }

        public KirasaModelsDBLib.GlobalProperties CtrlToDB(DllCuirassemProperties.Models.GlobalProperties e)
        {
            var config = new MapperConfiguration(cfg => {
                cfg.CreateMap<DllCuirassemProperties.Models.GlobalProperties, KirasaModelsDBLib.GlobalProperties>()

                   .ForPath(dest => dest.Common.CenterLatitude, map => map.MapFrom(src => src.Common.CenterZoneLatitude))
                   .ForPath(dest => dest.Common.CenterLongitude, map => map.MapFrom(src => src.Common.CenterZoneLongitude))
                   .ForPath(dest => dest.Common.ZoneAlarm, map => map.MapFrom(src => src.Common.ZoneAlarm))
                   .ForPath(dest => dest.Common.ZoneAttention, map => map.MapFrom(src => src.Common.ZoneAttention))
                   .ForPath(dest => dest.Common.OperatorLatitude, map => map.MapFrom(src => src.Common.OperatorLocationLatitude))
                   .ForPath(dest => dest.Common.OperatorLongitude, map => map.MapFrom(src => src.Common.OperatorLocationLongitude))

                   .ForPath(dest => dest.RadioIntelegence.FrequencyAccuracy, opts => opts.MapFrom(src => src.RadioIntelegence.FrequencyAccuracy))
                   .ForPath(dest => dest.RadioIntelegence.BandAccuracy, opts => opts.MapFrom(src => src.RadioIntelegence.BandAccuracy))
                   .ForPath(dest => dest.RadioIntelegence.SamplingFrequency, opts => opts.MapFrom(src => src.RadioIntelegence.SamplingFrequency))


                   .ForPath(dest => dest.KalmanFilterForDelay.StaticErrorMatrix, opts => opts.MapFrom(src => src.KalmanFilterForDelay.StaticArrayError))
                   .ForPath(dest => dest.KalmanFilterForDelay.DelayMeasurementError, opts => opts.MapFrom(src => src.KalmanFilterForDelay.DelayMeasurementError))
                   .ForPath(dest => dest.KalmanFilterForDelay.AccelerationNoise, opts => opts.MapFrom(src => src.KalmanFilterForDelay.NoiseAccelerationError))
                   .ForPath(dest => dest.KalmanFilterForDelay.TimeHoldToDetect, opts => opts.MapFrom(src => src.KalmanFilterForDelay.TimeHoldToDetectSource))
                   .ForPath(dest => dest.KalmanFilterForDelay.MaxResetTime, opts => opts.MapFrom(src => src.KalmanFilterForDelay.MaxResetTime))
                   .ForPath(dest => dest.KalmanFilterForDelay.MinAirObjPoints, opts => opts.MapFrom(src => src.KalmanFilterForDelay.MinPointObject))


                   .ForPath(dest => dest.KalmanFilterForCoordinate.StaticErrorMatrix, opts => opts.MapFrom(src => src.KalmanFilterForCoordinate.StaticArrayError))
                   .ForPath(dest => dest.KalmanFilterForCoordinate.SpeedUavMax, opts => opts.MapFrom(src => src.KalmanFilterForCoordinate.MaxUAVSpeed))
                   .ForPath(dest => dest.KalmanFilterForCoordinate.TimeHoldToDetect, opts => opts.MapFrom(src => src.KalmanFilterForCoordinate.TimeHoldToDetectSource))
                   .ForPath(dest => dest.KalmanFilterForCoordinate.RmsX, opts => opts.MapFrom(src => src.KalmanFilterForCoordinate.RmsX))
                   .ForPath(dest => dest.KalmanFilterForCoordinate.RmsY, opts => opts.MapFrom(src => src.KalmanFilterForCoordinate.RmsY))
                   .ForPath(dest => dest.KalmanFilterForCoordinate.RmsZ, opts => opts.MapFrom(src => src.KalmanFilterForCoordinate.RmsZ))
                   .ForPath(dest => dest.KalmanFilterForCoordinate.MaxResetTime, opts => opts.MapFrom(src => src.KalmanFilterForCoordinate.MaxResetTime))
                   .ForPath(dest => dest.KalmanFilterForCoordinate.CoordinateDefinitionError, opts => opts.MapFrom(src => src.KalmanFilterForCoordinate.CoordinateDefinitionError))
                   .ForPath(dest => dest.KalmanFilterForCoordinate.StrobeDistance, opts => opts.MapFrom(src => src.KalmanFilterForCoordinate.StrobeDistance))
                   .ForPath(dest => dest.KalmanFilterForCoordinate.TauError, opts => opts.MapFrom(src => src.KalmanFilterForCoordinate.TauError))
                   .ForPath(dest => dest.KalmanFilterForCoordinate.XyzError, opts => opts.MapFrom(src => src.KalmanFilterForCoordinate.XYZError))
                   .ForPath(dest => dest.KalmanFilterForCoordinate.MinAirObjPoints, opts => opts.MapFrom(src => src.KalmanFilterForCoordinate.MinPointAirObject))



                   .ForPath(dest => dest.Correlation.AmountCorrelationQuery, opts => opts.MapFrom(src => src.Correlation.AmountCorrelationQuery))
                   .ForPath(dest => dest.Correlation.PathThroughCorrelationThreshold, opts => opts.MapFrom(src => src.Correlation.PathThroughCorrelationThreshold))
                   .ForPath(dest => dest.Correlation.CoefficientBorderDеlayEstimation, opts => opts.MapFrom(src => src.Correlation.CoefficientBorderDеlayEstimation))

                   .ForPath(dest => dest.DRMFilters.IsImaginarySolution, opts => opts.MapFrom(src => src.DRMFilters.IsImSolution))
                   .ForPath(dest => dest.DRMFilters.StartElevation, opts => opts.MapFrom(src => src.DRMFilters.StartElevation))
                   .ForPath(dest => dest.DRMFilters.Alpha, opts => opts.MapFrom(src => src.DRMFilters.Alpha))
                   ;
                   });

            IMapper iMapper = config.CreateMapper();

            var destination = iMapper.Map<DllCuirassemProperties.Models.GlobalProperties, KirasaModelsDBLib.GlobalProperties>(e);
            return destination;
        }
    }
}
