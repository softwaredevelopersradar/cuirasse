﻿using InheritorsEventArgs;
using KirasaModelsDBLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using TableEvents;
using TableOperations;

namespace Cuirasse
{
    public partial class MainWindow
    {
        private void OnUpTable_TableUAVRes(object sender, TableEventArgs<TableUAVRes> e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                lUAVRes = (from t in e.Table let a = t as TableUAVBase select a).ToList();
                   
                if (lUAVRes.Count == 0)
                {
                    ucUAVResNew.UpdateUAVRes(lUAVRes);
                    ucMap.UpdateMapTrajectories(lUAVRes, lUAVTrajectory);
                    ucMap.ClearEllipse();
                }
                else
                {
                    ucUAVResNew.AddUAVRes(lUAVRes);
                    //Console.WriteLine($"Count drones: {lUAVRes.Count}");
                    //ucUAVResNew.ColorRowState(lUAVRes);
                    ucMap.UpdateMapTrajectories(lUAVRes, lUAVTrajectory);
                }

                /*if (toggleButton3D.IsChecked.Value)
                {
                    if (lUAVRes.Count > 0 && lUAVTrajectory.Count > 0 || lUAVRes.Count == 0 && lUAVTrajectory.Count == 0)
                        window3D.SendTo3DRework(lUAVRes, lUAVTrajectory);
                }*/

                SendRDMTrajTableToTechWindow(lUAVRes, lUAVTrajectory);
            });
        }
 
        private void OnUpTable_TableUAVTrajectory(object sender, TableEventArgs<TableUAVTrajectory> e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                lUAVTrajectory = (from t in e.Table let a = t as TableTrajectory select a).ToList();
                List<TableTrajectory> list = lUAVTrajectory.Where(x => x.TableUAVResId == PropNumUAV.SelectedIdUAVRes).ToList();
                //IEnumerable<TableUAVTrajectory> SortNum = list.OrderBy(x => x.Num); // сортировка по номеру точки

                if (list.Count == 0)
                {
                    //ucUAVRes.UpdateUAVTrajectory(list);
                }
                else
                {
                    // TEST
                    //Random rand = new Random();
                    //for (int i = 0; i < lUAVTrajectory.Count; i++)
                    //{
                    //    lUAVTrajectory[i].BandKHz = Convert.ToSingle(rand.Next(1000, 2000));
                    //}
                    //-----------------------------------------

                    // TEST /////////////////////
                    this.lUAVTrajectory = this.lUAVTrajectory.Where(x => x.FrequencyKHz > 0).ToList();
                    /////////////////////// TEST 
                    ucUAVResNew.AddUAVTrajectory(lUAVTrajectory);
                    ucUAVResNew.AddUAVRes(ucUAVResNew.UpdateTrackTimeUAVRes(lUAVTrajectory));
                    //ucUAVRes.AddUAVTrajectory(SortNum.ToList());
                    ucMap.UpdateMapTrajectories(lUAVRes, lUAVTrajectory);
                }
/*
                if (toggleButton3D.IsChecked.Value)
                {
                    if (lUAVRes.Count > 0 && lUAVTrajectory.Count > 0 || lUAVRes.Count == 0 && lUAVTrajectory.Count == 0)
                        window3D.SendTo3DRework(lUAVRes, lUAVTrajectory);
                }*/
                SendRDMTrajTableToTechWindow(lUAVRes, lUAVTrajectory);                               
            });
        }

        private void OnDeleteRecord_TableUAVRes(object sender, TableUAVRes e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                int ind = lUAVRes.FindIndex(x => x.Id == e.Id);
                if (ind != -1)
                {
                    lUAVRes.RemoveAt(ind);
                    ucUAVResNew.DeleteUAVRes(e);
                    ucMap.UpdateMapTrajectories(lUAVRes, lUAVTrajectory);
                }

                /*if (toggleButton3D.IsChecked.Value)
                {
                    if (lUAVRes.Count > 0 && lUAVTrajectory.Count > 0 || lUAVRes.Count == 0 && lUAVTrajectory.Count == 0)
                        window3D.SendTo3DRework(lUAVRes, lUAVTrajectory);
                }*/
                SendRDMTrajTableToTechWindow(lUAVRes, lUAVTrajectory);
            });
        }

        private void OnUpTable_TableUAVResArchive(object sender, TableEventArgs<TableUAVResArchive> e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                lUAVResArchive = (from t in e.Table let a = t as TableUAVBase select a).ToList();
 
                ucUAVResArchive.UpdateUAVResArchive(lUAVResArchive);

                ucMap.UpdateMapTrajectoriesArchive(lUAVResArchive, lUAVTrajectoryArchive);

                if (IsRemoveRange)
                {
                    wndViewModel.clientDB.Tables[NameTable.TableUAVResArchive].Add(NewTableUAVRes);
                    wndViewModel.clientDB.Tables[NameTable.TableUAVTrajectoryArchive].AddRange(lNewTrack);
                    IsRemoveRange = false;
                }
            });
        }

        private void OnUpTable_TableUAVTrajectoryArchive(object sender, TableEventArgs<TableUAVTrajectoryArchive> e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                lUAVTrajectoryArchive = (from t in e.Table let a = t as TableTrajectory select a).ToList();
                
                if(lUAVTrajectoryArchive.Count == 0)
                    ucUAVResArchive.UpdateUAVTrajectoryArchive(lUAVTrajectoryArchive);

                if (lUAVResArchive.Count == 1)
                {
                    List<TableTrajectory> list = lUAVTrajectoryArchive.Where(x => x.TableUAVResId == lUAVResArchive[0].Id).ToList();
                    ucUAVResArchive.UpdateUAVTrajectoryArchive(list);
                }
            });
        }

        private void OnUpTable_TableRemotePoints(object sender, TableEventArgs<TableRemotePoints> e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                lRemotePoints = (from t in e.Table let a = t as TableReceivingPoint select a).ToList();
                ucRemotePoints.UpdateReceivingPoint(lRemotePoints);
                ucMap.UpdateMapReceivingPoint(lRemotePoints, NameTable.TableRemotePoints);
            });
        }

        private void OnUpTable_TableLocalPoints(object sender, TableEventArgs<TableLocalPoints> e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                lLocalPoints = (from t in e.Table let a = t as TableReceivingPoint select a).ToList();
                ucLocalPoints.UpdateReceivingPoint(lLocalPoints);
                ucMap.UpdateMapReceivingPoint(lLocalPoints, NameTable.TableLocalPoints);
                SendTableStationToTechWindow(lLocalPoints);
            });
        }

        private void OnUpTable_TableOtherPoints(object sender, TableEventArgs<TableOtherPoints> e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                lOtherPoints = e.Table;
                ucOtherPoints.UpdateOtherPoints(lOtherPoints);

                DrawLocationGrozaS(lOtherPoints);
            });
        }

        private void OnUpTable_TableFreqRangesRecon(object sender, TableEventArgs<TableFreqRangesRecon> e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                lFreqRangesRecon = (from t in e.Table let a = t as TableFreqRanges select a).ToList();
                ucFreqRangesRecon.UpdateFreqRanges(lFreqRangesRecon);
                PanoramaImportRIFreqs(lFreqRangesRecon);
            });
        }

        private void OnUpTable_TableFreqKnown(object sender, TableEventArgs<TableFreqKnown> e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                lFreqKnown = (from t in e.Table let a = t as TableFreqRanges select a).ToList();
                ucFreqKnown.UpdateFreqRanges(lFreqKnown);
            });
        }

        private void OnUpTable_TableAeroscope(object sender, TableEventArgs<TableAeroscope> e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                lAeroscope = e.Table;
                if (lAeroscope.Count == 0)
                {
                    ucAeroscope.UpdateAeroscope(lAeroscope);
                    ucMap.UpdateMapTrajectories(lAeroscope, lATrajectory);
                }
                else
                {
                    ucAeroscope.AddAeroscope(lAeroscope);
                    ucMap.UpdateMapTrajectories(lAeroscope, lATrajectory);
                }
/*
                if (toggleButton3D.IsChecked.Value)
                    window3D.SendTo3DTest(lAeroscope, lATrajectory);*/

                SendAeroTableToTechWindow(lAeroscope, lATrajectory);
            });
        }

        private void OnUpTable_TableAeroscopeTrajectory(object sender, TableEventArgs<TableAeroscopeTrajectory> e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                lATrajectory = e.Table;
                List<TableAeroscopeTrajectory> list = lATrajectory.Where(x => x.SerialNumber == PropNumUAV.SelectedSerialNumAeroscope).ToList();
                //IEnumerable<TableAeroscopeTrajectory> SortNum = list.OrderBy(x => x.Num); // сортировка по номеру точки

                if (lATrajectory.Count == 0)
                {
                   
                }
                else
                {
                    ucAeroscope.AddATrajectory(lATrajectory);
                    //ucAeroscope.AddATrajectory(SortNum.ToList());
                    ucMap.UpdateMapTrajectories(lAeroscope, lATrajectory);
                }
/*
                if (toggleButton3D.IsChecked.Value)
                    window3D.SendTo3DTest(lAeroscope, lATrajectory);
*/
                SendAeroTableToTechWindow(lAeroscope, lATrajectory);
            });

            //Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            //{
            //    lATrajectory = e.Table;
            //    List<TableAeroscopeTrajectory> list = lATrajectory.Where(x => x.SerialNumber== PropNumUAV.SelectedSerialNumAeroscope).ToList();

            //    if (lATrajectory.Count == 0)
            //        ucAeroscope.UpdateATrajectory(lATrajectory);
            //    else
            //    {
            //        ucAeroscope.AddATrajectory(list);
            //        ucMap.UpdateMapTrajectories(lAeroscope, lATrajectory);
            //    }

            //    if (toggleButton3D.IsChecked.Value)
            //        window3D.SendTo3DTest(lAeroscope, lATrajectory);
            //});
        }

        private void OnDeleteRecord_TableAeroscope(object sender, TableAeroscope e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                int ind = lAeroscope.FindIndex(x => x.SerialNumber == e.SerialNumber);
                if (ind != -1)
                {
                    lAeroscope.RemoveAt(ind);
                    ucAeroscope.DeleteAeroscope(e);
                    ucMap.UpdateMapTrajectories(lAeroscope, lATrajectory);
                }
/*
                if (toggleButton3D.IsChecked.Value)
                    window3D.SendTo3DTest(lAeroscope, lATrajectory);*/
            });
        }

        private void HandlerUpdate_GlobalProperties(object sender, TableEventArgs<GlobalProperties> e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                wndViewModel.GlobalPropertiesModel = e.Table.FirstOrDefault();

                SetGlobalProperties();

                DrawLocationOperator(wndViewModel.GlobalPropertiesModel);
                //DrawZones();
            });
        }

        private void OnUpTable_TableSignalsUAV(object sender, TableEventArgs<TableSignalsUAV> e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            { 
                lSignalsUAV = e.Table;
            
                if (lSignalsUAV.Count == 0)
                    ucSignalsUAV.UpdateSignalsUAV(lSignalsUAV);
                else ucSignalsUAV.AddSignalsUAV(lSignalsUAV);


            });
        }

        private void OnDeleteRecord_TableSignalsUAV(object sender, TableSignalsUAV e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                int ind = lSignalsUAV.FindIndex(x => x.Id == e.Id);
                if (ind != -1)
                {
                    lSignalsUAV.RemoveAt(ind);
                    ucSignalsUAV.DeleteSignalsUAV(e);
                }
            });
        }

        private async void UcSignalsUAV_OnGetTypeL(object sender, TableSignalsUAV e)
        {
            var answer = await client.SendSignalMessage(e.Id, e.FrequencyKHz, e.BandKHz, DAPprotocols.RangeType.RecognitionL, DAPprotocols.TableType.Signals);
        }

        private async void UcSignalsUAV_OnGetTypeM(object sender, TableSignalsUAV e)
        {
            var answer = await client.SendSignalMessage(e.Id, e.FrequencyKHz, e.BandKHz, DAPprotocols.RangeType.RecognitionM, DAPprotocols.TableType.Signals);
        }

        private async void LoadTables()
        {
            try
            {
                #region UAVRes
                lUAVRes = await wndViewModel.clientDB.Tables[NameTable.TableUAVRes].LoadAsync<TableUAVBase>();
                ucUAVResNew.UpdateUAVRes(lUAVRes);

                lUAVTrajectory = await wndViewModel.clientDB.Tables[NameTable.TableUAVTrajectory].LoadAsync<TableTrajectory>();
                ucMap.UpdateMapTrajectories(lUAVRes, lUAVTrajectory);
                ucUAVResNew.AddUAVTrajectory(lUAVTrajectory);
                ucUAVResNew.AddUAVRes(ucUAVResNew.UpdateTrackTimeUAVRes(lUAVTrajectory));

                SendRDMTrajTableToTechWindow(lUAVRes, lUAVTrajectory);
                #endregion

                #region UAVSignals
                lSignalsUAV = await wndViewModel.clientDB.Tables[NameTable.TableSignalsUAV].LoadAsync<TableSignalsUAV>();
                ucSignalsUAV.UpdateSignalsUAV(lSignalsUAV);
                #endregion

                #region UAVResArchive
                lUAVResArchive = await wndViewModel.clientDB.Tables[NameTable.TableUAVResArchive].LoadAsync<TableUAVBase>();
                List<TableUAVResArchive> listArchive = new List<TableUAVResArchive>();
                for (int i = 0; i < lUAVResArchive.Count; i++)
                {
                    if (lUAVResArchive[i].IsActive)
                    {
                        lUAVResArchive[i].IsActive = false;
                    }
                    listArchive.Add(lUAVResArchive[i].ToUAVResArchive());
                }
                wndViewModel.clientDB.Tables[NameTable.TableUAVResArchive].AddRange(listArchive);

                lUAVTrajectoryArchive = await wndViewModel.clientDB.Tables[NameTable.TableUAVTrajectoryArchive].LoadAsync<TableTrajectory>();
                List<TableTrajectory> list = lUAVTrajectoryArchive.Where(x => x.TableUAVResId == PropNumUAV.SelectedIdUAVResArchive).ToList();
                ucUAVResArchive.UpdateUAVTrajectoryArchive(list);
                #endregion

                #region Aeroscope
                lAeroscope = await wndViewModel.clientDB.Tables[NameTable.TableАeroscope].LoadAsync<TableAeroscope>();
                ucAeroscope.UpdateAeroscope(lAeroscope);

                lATrajectory = await wndViewModel.clientDB.Tables[NameTable.TableАeroscopeTrajectory].LoadAsync<TableAeroscopeTrajectory>();
                ucMap.UpdateMapTrajectories(lAeroscope, lATrajectory);

                SendAeroTableToTechWindow(lAeroscope, lATrajectory);
                #endregion

                #region ReceivingPoints
                lRemotePoints = await wndViewModel.clientDB.Tables[NameTable.TableRemotePoints].LoadAsync<TableReceivingPoint>();
                ucRemotePoints.UpdateReceivingPoint(lRemotePoints);
                ucMap.UpdateMapReceivingPoint(lRemotePoints, NameTable.TableRemotePoints);

                lLocalPoints = await wndViewModel.clientDB.Tables[NameTable.TableLocalPoints].LoadAsync<TableReceivingPoint>();
                ucLocalPoints.UpdateReceivingPoint(lLocalPoints);
                ucMap.UpdateMapReceivingPoint(lLocalPoints, NameTable.TableLocalPoints);
                ucMap.CenteringMap();

                SendTableStationToTechWindow(lLocalPoints);
                #endregion

                #region FreqRanges
                lFreqKnown = await wndViewModel.clientDB.Tables[NameTable.TableFreqKnown].LoadAsync<TableFreqRanges>();
                ucFreqKnown.UpdateFreqRanges(lFreqKnown);

                lFreqRangesRecon = await wndViewModel.clientDB.Tables[NameTable.TableFreqRangesRecon].LoadAsync<TableFreqRanges>();
                ucFreqRangesRecon.UpdateFreqRanges(lFreqRangesRecon);
                PanoramaImportRIFreqs(lFreqRangesRecon);
                #endregion

                #region Properties
                wndViewModel.GlobalPropertiesModel = (await wndViewModel.clientDB.Tables[NameTable.GlobalProperties].LoadAsync<GlobalProperties>()).FirstOrDefault();
                SetGlobalProperties();
                /*GlobalPropertyTo3D(wndViewModel.GlobalPropertiesModel);*/
                
                DrawLocationOperator(wndViewModel.GlobalPropertiesModel);

                SendTableGlobalPropertiesToTechWindow(wndViewModel.GlobalPropertiesModel);

                //DrawZones();
                #endregion

                #region OtherPoints
                lOtherPoints = await wndViewModel.clientDB.Tables[NameTable.TableOtherPoints].LoadAsync<TableOtherPoints>();
                ucOtherPoints.UpdateOtherPoints(lOtherPoints);

                // TEST -------------------------------------------------- 
                //TableOtherPoints table = new TableOtherPoints
                //{ 
                //    Id = 1,
                //    Type = TypeOtherPoints.GrozaS,
                //    Coordinates = new Coord
                //    {
                //        Latitude = 53.7899777,
                //        Longitude = 27.77565,
                //        Altitude = 150
                //    }
                //};
                //lOtherPoints.Add(table);
                // -------------------------------------------------- TEST 
                DrawLocationGrozaS(lOtherPoints);
                #endregion
            }
            catch (ClientDataBase.ExceptionClient exeptClient)
            {
                MessageBox.Show(exeptClient.Message);
            }
            catch (ClientDataBase.ExceptionDatabase excpetService)
            {
                MessageBox.Show(excpetService.Message);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
