﻿using ClientDataBase;
using InheritorsEventArgs;
using System;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Media;

namespace Cuirasse
{
    public partial class MainWindow
    {
        private void HandlerErrorDataBase(object sender, OperationTableEventArgs eventArgs)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                MessageBox.Show(eventArgs.GetMessage);
            });
        }

        private void HandlerUpData(object sender, DataEventArgs eventArgs)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {

            });
        }

        private void HandlerDisconnect(object sender, ClientEventArgs eventArgs)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                if (eventArgs.GetMessage != "")
                {
                    DbControlConnection.ShowDisconnect();
                }
                    //clientDB = null;
            });
        }

        private void HandlerConnect(object sender, ClientEventArgs e)
        {
            DbControlConnection.ShowConnect();
            LoadTables();
          

        }

        private void DbConnection_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (wndViewModel.clientDB != null && wndViewModel.clientDB.IsConnected())
                    DisconnectClientDB();
                else
                {
                    ConnectClientDB();
                }
            }
            catch (ClientDataBase.ExceptionClient exceptClient)
            {
                MessageBox.Show(exceptClient.Message);
            }

            //try
            //{
            //    if (clientDB != null)
            //        clientDB.Disconnect();
            //    else
            //    {
            //        //string endPoint = "192.168.1.11:8302"; // Aeroscope
            //        clientDB = new ClientDB(this.Name, endPoint);
            //        InitClientDB();
            //        clientDB.ConnectAsync();
            //    }
            //}
            //catch (ClientDataBase.ExceptionClient exceptClient)
            //{
            //    HandlerDisconnect(this, null);
            //    MessageBox.Show(exceptClient.Message);
            //}
            }
        }
}
