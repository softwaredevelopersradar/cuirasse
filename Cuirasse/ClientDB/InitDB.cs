﻿using System;
using System.Windows;
using ClientDataBase;
using InheritorsEventArgs;
using KirasaModelsDBLib;

namespace Cuirasse
{
    public partial class MainWindow 
    {
        //ClientDB clientDB;
        string endPoint = "127.0.0.1:8302";

        private void ConnectClientDB()
        {
            if (wndViewModel.clientDB != null)
                DisconnectClientDB();

            wndViewModel.clientDB = new ClientDB(this.Name, endPoint);

            wndViewModel.clientDB.OnConnect += HandlerConnect;
            wndViewModel.clientDB.OnDisconnect += HandlerDisconnect;
            wndViewModel.clientDB.OnUpData += HandlerUpData;
            wndViewModel.clientDB.OnErrorDataBase += HandlerErrorDataBase;

            (wndViewModel.clientDB.Tables[NameTable.TableUAVRes] as ITableUpdate<TableUAVRes>).OnUpTable += OnUpTable_TableUAVRes;
            (wndViewModel.clientDB.Tables[NameTable.TableUAVRes] as ITableUpRecord<TableUAVRes>).OnDeleteRecord += OnDeleteRecord_TableUAVRes;
            (wndViewModel.clientDB.Tables[NameTable.TableUAVTrajectory] as ITableUpdate<TableUAVTrajectory>).OnUpTable += OnUpTable_TableUAVTrajectory;
            (wndViewModel.clientDB.Tables[NameTable.TableLocalPoints] as ITableUpdate<TableLocalPoints>).OnUpTable += OnUpTable_TableLocalPoints;
            (wndViewModel.clientDB.Tables[NameTable.TableRemotePoints] as ITableUpdate<TableRemotePoints>).OnUpTable += OnUpTable_TableRemotePoints;
            (wndViewModel.clientDB.Tables[NameTable.TableOtherPoints] as ITableUpdate<TableOtherPoints>).OnUpTable += OnUpTable_TableOtherPoints;
            (wndViewModel.clientDB.Tables[NameTable.TableFreqKnown] as ITableUpdate<TableFreqKnown>).OnUpTable += OnUpTable_TableFreqKnown;
            (wndViewModel.clientDB.Tables[NameTable.TableFreqRangesRecon] as ITableUpdate<TableFreqRangesRecon>).OnUpTable += OnUpTable_TableFreqRangesRecon;
            //(wndViewModel.clientDB.Tables[NameTable.TableАeroscope] as ITableUpdate<TableAeroscope>).OnUpTable += OnUpTable_TableAeroscope;
            //(wndViewModel.clientDB.Tables[NameTable.TableАeroscope] as ITableUpRecord<TableAeroscope>).OnDeleteRecord += OnDeleteRecord_TableAeroscope;
            //(wndViewModel.clientDB.Tables[NameTable.TableАeroscopeTrajectory] as ITableUpdate<TableAeroscopeTrajectory>).OnUpTable += OnUpTable_TableAeroscopeTrajectory;
            (wndViewModel.clientDB.Tables[NameTable.GlobalProperties] as ITableUpdate<GlobalProperties>).OnUpTable += HandlerUpdate_GlobalProperties;
            (wndViewModel.clientDB.Tables[NameTable.TableUAVResArchive] as ITableUpdate<TableUAVResArchive>).OnUpTable += OnUpTable_TableUAVResArchive; 
            (wndViewModel.clientDB.Tables[NameTable.TableUAVTrajectoryArchive] as ITableUpdate<TableUAVTrajectoryArchive>).OnUpTable += OnUpTable_TableUAVTrajectoryArchive;
            (wndViewModel.clientDB.Tables[NameTable.TableSignalsUAV] as ITableUpdate<TableSignalsUAV>).OnUpTable += OnUpTable_TableSignalsUAV;
            (wndViewModel.clientDB.Tables[NameTable.TableSignalsUAV] as ITableUpRecord<TableSignalsUAV>).OnDeleteRecord += OnDeleteRecord_TableSignalsUAV;

            wndViewModel.clientDB.ConnectAsync();
        }

       

        private void DisconnectClientDB()
        {
            if (wndViewModel.clientDB != null)
            {
                wndViewModel.clientDB.Disconnect();

                wndViewModel.clientDB.OnConnect -= HandlerConnect;
                wndViewModel.clientDB.OnDisconnect -= HandlerDisconnect;
                wndViewModel.clientDB.OnUpData -= HandlerUpData;
                wndViewModel.clientDB.OnErrorDataBase -= HandlerErrorDataBase;

                (wndViewModel.clientDB.Tables[NameTable.TableUAVRes] as ITableUpdate<TableUAVRes>).OnUpTable -= OnUpTable_TableUAVRes;
                (wndViewModel.clientDB.Tables[NameTable.TableUAVRes] as ITableUpRecord<TableUAVRes>).OnDeleteRecord -= OnDeleteRecord_TableUAVRes;
                (wndViewModel.clientDB.Tables[NameTable.TableUAVTrajectory] as ITableUpdate<TableUAVTrajectory>).OnUpTable -= OnUpTable_TableUAVTrajectory;
                (wndViewModel.clientDB.Tables[NameTable.TableLocalPoints] as ITableUpdate<TableLocalPoints>).OnUpTable -= OnUpTable_TableLocalPoints;
                (wndViewModel.clientDB.Tables[NameTable.TableRemotePoints] as ITableUpdate<TableRemotePoints>).OnUpTable -= OnUpTable_TableRemotePoints;
                (wndViewModel.clientDB.Tables[NameTable.TableFreqKnown] as ITableUpdate<TableFreqKnown>).OnUpTable -= OnUpTable_TableFreqKnown;
                (wndViewModel.clientDB.Tables[NameTable.TableFreqRangesRecon] as ITableUpdate<TableFreqRangesRecon>).OnUpTable -= OnUpTable_TableFreqRangesRecon;
                //(wndViewModel.clientDB.Tables[NameTable.TableАeroscope] as ITableUpdate<TableAeroscope>).OnUpTable -= OnUpTable_TableAeroscope;
                //(wndViewModel.clientDB.Tables[NameTable.TableАeroscope] as ITableUpRecord<TableAeroscope>).OnDeleteRecord -= OnDeleteRecord_TableAeroscope;
                //(wndViewModel.clientDB.Tables[NameTable.TableАeroscopeTrajectory] as ITableUpdate<TableAeroscopeTrajectory>).OnUpTable -= OnUpTable_TableAeroscopeTrajectory;
                (wndViewModel.clientDB.Tables[NameTable.GlobalProperties] as ITableUpdate<GlobalProperties>).OnUpTable -= HandlerUpdate_GlobalProperties;
                (wndViewModel.clientDB.Tables[NameTable.TableUAVResArchive] as ITableUpdate<TableUAVResArchive>).OnUpTable -= OnUpTable_TableUAVResArchive;
                (wndViewModel.clientDB.Tables[NameTable.TableUAVTrajectoryArchive] as ITableUpdate<TableUAVTrajectoryArchive>).OnUpTable -= OnUpTable_TableUAVTrajectoryArchive;

                wndViewModel.clientDB = null;
            }
        }

     
        //void InitClientDB()
        //{
        //    clientDB.OnConnect += HandlerConnect;
        //    clientDB.OnDisconnect += HandlerDisconnect;
        //    clientDB.OnUpData += HandlerUpData;
        //    clientDB.OnErrorDataBase += HandlerErrorDataBase;

        //    (clientDB.Tables[NameTable.TableUAVRes] as ITableUpdate<TableUAVRes>).OnUpTable += OnUpTable_TableUAVRes;
        //    (clientDB.Tables[NameTable.TableUAVRes] as ITableUpRecord<TableUAVRes>).OnDeleteRecord += OnDeleteRecord_TableUAVRes;
        //    (clientDB.Tables[NameTable.TableUAVTrajectory] as ITableUpdate<TableUAVTrajectory>).OnUpTable += OnUpTable_TableUAVTrajectory;
        //    (clientDB.Tables[NameTable.TableLocalPoints] as ITableUpdate<TableLocalPoints>).OnUpTable += OnUpTable_TableLocalPoints;
        //    (clientDB.Tables[NameTable.TableRemotePoints] as ITableUpdate<TableRemotePoints>).OnUpTable += OnUpTable_TableRemotePoints;
        //    (clientDB.Tables[NameTable.TableFreqKnown] as ITableUpdate<TableFreqKnown>).OnUpTable += OnUpTable_TableFreqKnown;
        //    (clientDB.Tables[NameTable.TableFreqRangesRecon] as ITableUpdate<TableFreqRangesRecon>).OnUpTable += OnUpTable_TableFreqRangesRecon;
        //    (clientDB.Tables[NameTable.TableАeroscope] as ITableUpdate<TableAeroscope>).OnUpTable += OnUpTable_TableAeroscope;
        //    (clientDB.Tables[NameTable.TableАeroscope] as ITableUpRecord<TableAeroscope>).OnDeleteRecord += OnDeleteRecord_TableAeroscope;
        //    (clientDB.Tables[NameTable.TableАeroscopeTrajectory] as ITableUpdate<TableAeroscopeTrajectory>).OnUpTable += OnUpTable_TableAeroscopeTrajectory;
        //    (clientDB.Tables[NameTable.GlobalProperties] as ITableUpdate<GlobalProperties>).OnUpTable += HandlerUpdate_GlobalProperties;

        //}


    }
}
