﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Cuirasse
{
    public class PropertyModel : INotifyPropertyChanged
    {
        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }
        #endregion
   
        private byte modeLabel;
        public byte ModeLabel
        {
            get { return modeLabel; }
            set
            {
                //if (modeLabel != value)
                //{
                    modeLabel = value;
                    OnPropertyChanged();
                //}
            }
        }

        public byte ModeLabelCurrent { get; set; } // field for translation

        private byte modeLed;
        public byte ModeLed
        {
            get { return modeLed; }
            set
            {
                if (modeLed != value)
                {
                    modeLed = value;
                    OnPropertyChanged();
                }
            }
        }
        
        public PropertyModel()
        {
            ModeLabel = new byte();
            ModeLed = new byte();
            ModeLabelCurrent = new byte();
        }


    }
}
