﻿using ClientDataBase;
using DllCuirassemProperties.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Cuirasse
{
    public class WndViewModel : INotifyPropertyChanged
    {
        #region PropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));

        }
        #endregion

        public ClientDB clientDB;

        private PropertyModel propertyModel;
        public PropertyModel PropertyModel
        {
            get { return propertyModel; }
            set
            {
                if (propertyModel != value)
                {
                    propertyModel = value;
                    OnPropertyChanged();
                }
            }
        }

        private LocalProperties localPropertiesModel;
        public LocalProperties LocalPropertiesModel
        {
            get { return localPropertiesModel; }
            set
            {
                if (localPropertiesModel != value)
                {
                    localPropertiesModel = value;
                    OnPropertyChanged();
                }
            }
        }

        private KirasaModelsDBLib.GlobalProperties globalPropertiesModel;
        public KirasaModelsDBLib.GlobalProperties GlobalPropertiesModel
        {
            get { return globalPropertiesModel; }
            set
            {
                if (globalPropertiesModel != value)
                {
                    globalPropertiesModel = value;
                    OnPropertyChanged();
                }
            }
        }

        public WndViewModel()
        {
            Thread.CurrentThread.CurrentUICulture = new CultureInfo("en");

            GlobalPropertiesModel = new KirasaModelsDBLib.GlobalProperties();
            LocalPropertiesModel = new LocalProperties();

            PropertyModel = new PropertyModel();
        }      
    }
}
