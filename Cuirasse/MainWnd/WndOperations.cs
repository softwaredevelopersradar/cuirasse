﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Cuirasse
{
    public partial class MainWindow : Window
    {
        private void UpdatePropertyModel()
        {
            wndViewModel.PropertyModel.ModeLabel = wndViewModel.PropertyModel.ModeLabelCurrent;
        }
    }
}
