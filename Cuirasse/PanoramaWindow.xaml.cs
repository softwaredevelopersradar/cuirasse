﻿using DAPServerClient2;
using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Cuirasse
{
    /// <summary>
    /// Логика взаимодействия для Panorama.xaml
    /// </summary>
    public partial class PanoramaWindow : Window
    {        
        public event EventHandler OnClickBClose;

        private const int DEFAULT_DESCTOP_DPI = 96;

       

        public PanoramaWindow()
        {
            InitializeComponent();            
        }

        private void bClose_Click(object sender, RoutedEventArgs e)
        {
            Hide();
            OnClickBClose?.Invoke(sender, e);
        }

        private void Window_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }
        
        private void PanoramasWindow_Closed(object sender, EventArgs e)
        {
            Hide();
            OnClickBClose?.Invoke(sender, e);
        }

        private void bMax_Click(object sender, RoutedEventArgs e)
        {            
            ChangeImage();
        }

        private void bMin_Click(object sender, RoutedEventArgs e)
        {
            PanoramasWindow.WindowState = WindowState.Minimized;
        }

        private void lResize_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                ChangeImage();
            }
            else { }
        }

        /// <summary>
        /// Изменение значка кнопки в зависимости от размера окна
        /// </summary>
        private void ChangeImage()
        {
            Rect rect = SystemParameters.WorkArea;
            if (PanoramasWindow.WindowState == WindowState.Maximized)
            {
                PanoramasWindow.WindowState = WindowState.Normal;
                iMax.Source = new BitmapImage(new Uri(@"pack://application:,,,/"
                                    + System.Reflection.Assembly.GetExecutingAssembly().GetName().Name
                                    + ";component/"
                                    + "Resources/Resize.png", UriKind.Absolute));
            }
            else
            {
                PanoramasWindow.WindowState = WindowState.Maximized;

                //IntPtr intPtr = new WindowInteropHelper(App.Current.MainWindow).Handle;
                //System.Drawing.Graphics desktop = System.Drawing.Graphics.FromHwnd(intPtr);
                //var graphics = System.Drawing.Graphics.FromHwnd(IntPtr.Zero);
                //Width = desktop.DpiX / DEFAULT_DESCTOP_DPI;
                //Height = desktop.DpiY / DEFAULT_DESCTOP_DPI;

                //var pixelToDpi = 96.0 / graphics.
                //Width = rect.Size.Width;
                //Height = rect.Size.Height;
                //Top = rect.Top;
                //Left = rect.Left;
                iMax.Source = new BitmapImage(new Uri(@"pack://application:,,,/"
                                    + System.Reflection.Assembly.GetExecutingAssembly().GetName().Name
                                    + ";component/"
                                    + "Resources/Max.png", UriKind.Absolute));
            }
        }        
    }
}
