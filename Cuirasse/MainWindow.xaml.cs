﻿using System;
using System.ComponentModel;
using System.Net;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using ValuesCorrectLib;

namespace Cuirasse
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }
        #endregion

        WndViewModel wndViewModel;

        public MainWindow()
        {
            InitializeComponent();

            wndViewModel = new WndViewModel();
            DataContext = wndViewModel;

            basicProperties.SetPassword("1111");

            InitMarkup();
            InitLocalProperties();
            InitTextBoxManager();
            InitCorrGraphProperties();
            InitDefaultChannels();
            InitPLibrary2();
            InitDAPClientEvents();            
            InitPLIbraryEvents();
            InitPanoramsGraph();
            InitPanorama();
            InitTables();
            InitMap();
            InitComboxSettings();
            ConnectClientDB();
           
            SetLanguageTables(basicProperties.Local.Common.Language);
            TranslatorTables.LoadDictionary(basicProperties.Local.Common.Language);
            SetLanguagePanoramas(basicProperties.Local.Common.Language);
            PanoramasGraphs_Access(basicProperties.Local.Common.Access);
            ChangeTechWindowLaguage(basicProperties.Local.Common.Language);
            /*Change3DWindowLanguage(basicProperties.Local.Common.Language);

            InitWindow3D();
*/
            InitGainWindow();
            InitTechWindow();
        }

        private void Window_Closed(object sender, System.EventArgs e)
        {
            System.Windows.Threading.Dispatcher.ExitAllFrames();
        }

        private void ScrollViewer_PreviewMouseWheel(object sender, System.Windows.Input.MouseWheelEventArgs e)
        {
            try
            {
                System.Windows.Controls.ScrollViewer scroll = sender as System.Windows.Controls.ScrollViewer;
                if (scroll != null)
                {
                    if (e.Delta > 0)
                    {
                        scroll.LineRight();
                    }
                    else
                        scroll.LineLeft();
                }
                e.Handled = true;
            }
            catch { }
        }

        private void ESToggleButton_Click(object sender, RoutedEventArgs e)
        {
            var toggleButton = sender as ToggleButton;
            if (toggleButton is null)
            {
                toggleButton.IsChecked = false;
                return;
            }

            try
            {
                if (toggleButton.IsChecked.Value)
                {
                    this.esClient = new EScopeProcessor.EScopeMain();
                    this.esClient.EScopeProcessor_OnParseFinishedSuccessfully += EsMainClient_EScopeProcessor_OnParseFinishedSuccessfully;
                    // temporary
                    /*this.esClient = new EScopeLib.ScopeProcessor();

                    this.esClient.Connect();
                    this.esClient.OnObtainResult += EsClient_OnObtainResult;*/
                }
                else
                {
                    if (this.esClient == null)
                        return;

                    this.esClient.EScopeProcessor_OnParseFinishedSuccessfully -= EsMainClient_EScopeProcessor_OnParseFinishedSuccessfully;
                    this.esClient.Dispose();

                    /*if (this.esClient != null)
                        this.esClient.Disconnect();

                    this.esClient.OnObtainResult -= EsClient_OnObtainResult;*/
                }
            }
            catch (Exception ex)
            {
                toggleButton.IsChecked = false;

                if (ex.Message.Contains("The requested address is not valid in its context"))
                    MessageBox.Show(ex.Message + "\n\nOne of the most possible way to solve problem is to " +
                                                        "connect the device to computer, cause the UdpClient needs " +
                                                        "an udp connection between pc and receiver.");
                else
                    MessageBox.Show(ex.Message);
            }
        }

        private void ASToggleButton_Click(object sender, RoutedEventArgs e)
        {
            var toggleButton = sender as ToggleButton;
            if (toggleButton is null)
            {
                toggleButton.IsChecked = false;
                return;
            }

            try
            {
                if (toggleButton.IsChecked.Value)
                {
                    this.asClient = new UDPASReceiver.Client();
                    this.asClient.OnUDPPacketReceived += AsClient_OnUDPPacketReceived;
                }
                else
                {
                    if (this.asClient != null)
                        this.asClient.Disconnect();

                    this.asClient.OnUDPPacketReceived -= AsClient_OnUDPPacketReceived;
                    this.asClient = null;
                }
            }
            catch (Exception ex)
            {
                toggleButton.IsChecked = false;
                MessageBox.Show(ex.Message);
            }
        }
    }
}
